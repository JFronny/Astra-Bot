namespace Bot.Converters
{
    public enum RpsOption
    {
        Rock,
        Paper,
        Scissor
    }
}