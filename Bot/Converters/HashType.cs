namespace Bot.Converters
{
    public enum HashType
    {
        Sha1,
        Sha256,
        Md5
    }
}