using System;
using System.Linq;
using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Converters;
using DSharpPlus.Entities;

namespace Bot.Converters
{
    public class EnumConv<T> : IArgumentConverter<T> where T : struct, Enum
    {
        public Task<Optional<T>> ConvertAsync(string value, CommandContext ctx)
        {
            string tmp1 = value.ToLower();
            return Task.FromResult(Optional.FromValue(Enum.GetValues<T>().First(
                s =>
                {
                    string tmp2 = s.ToString().ToLower();
                    return tmp2.StartsWith(tmp1) || tmp2.EndsWith(tmp1) || tmp1.StartsWith(tmp2) || tmp1.EndsWith(tmp2);
                })));
        }
    }
}