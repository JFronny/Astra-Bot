﻿#if DEBUG
#define VERBOSE
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bot.Commands.Administration;
using Bot.Config;
using Bot.Config.Data;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Exceptions;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Enums;
using DSharpPlus.Interactivity.Extensions;
using Microsoft.Extensions.Logging;
using Octokit;
using Shared;

namespace Bot
{
    public static class Program
    {
        public static EventId Astra = new(99, "Astra");
        public static readonly Random Rnd = new();
        public static DiscordClient? Client;
        public static GitHubClient? Github;
        public static Perspective? Perspective;
        public static DateTime Start = DateTime.Now;
        public static bool Exit;
        private static CommandsNextExtension? Commands { get; set; }

        [STAThread]
        private static async Task Main()
        {
            Console.WriteLine("Initializing");
#if NO_TIMED_BAN || NO_NSFW || NO_CLI || VERBOSE
            Console.WriteLine("Build config:");
#if NO_TIMED_BAN
            Console.WriteLine("- NO_TIMED_BAN");
#endif
#if NO_NSFW
            Console.WriteLine("- NO_NSFW");
#endif
#if NO_CLI
            Console.WriteLine("- NO_CLI");
#endif
#if VERBOSE
            Console.WriteLine("- VERBOSE");
#endif
#endif
            Github = new GitHubClient(new ProductHeaderValue("AstraBot"));
            Perspective = new Perspective(TokenData.Get().PerspectiveToken);
            DiscordConfiguration cfg = new()
            {
                Token = TokenData.Get().DiscordToken,
                TokenType = TokenType.Bot,
                AutoReconnect = true,
#if VERBOSE
                MinimumLogLevel = LogLevel.Debug
#else
                MinimumLogLevel = LogLevel.Information
#endif
            };
            Client = new DiscordClient(cfg);
            Commands = Client.UseCommandsNext(new CommandsNextConfiguration
            {
                StringPrefixes = new string[0],
                EnableDms = true,
                PrefixResolver = ResolvePrefix
            });
            Commands.CommandExecuted += Commands_CommandExecuted;
            Commands.CommandErrored += Commands_CommandErrored;
            Client.UseInteractivity(new InteractivityConfiguration
            {
                PaginationBehaviour = PaginationBehaviour.Ignore,
                Timeout = TimeSpan.FromMinutes(2)
            });
            await Commands.RegisterAll();
            Client.Ready += ClientReady;
            Client.MessageCreated += MessageCreated;
            Client.ClientErrored += ClientClientErrored;
            await Client.ConnectAsync();
            ProcessLoop pl = new(Client);
            while (!Exit)
                Exit = await pl.RunIteration();
            await Client.DisconnectAsync();
            Client.Dispose();
            Client = null;
        }

        private static async Task<int> ResolvePrefix(DiscordMessage msg)
        {
            if (msg.Author.IsBot)
                return -1;
            string prefix = msg.Channel.GetPrefix();
            string content = msg.Content.TrimStart(' ');
            if (content.StartsWith(prefix))
            {
                if (content.StartsWith($"{prefix} "))
                    return prefix.Length + 1;
                return prefix.Length;
            }
            if (content.StartsWith(Client.CurrentUser.Mention))
            {
                if (content.StartsWith($"{Client.CurrentUser.Mention} "))
                    return Client.CurrentUser.Mention.Length + 1;
                return Client.CurrentUser.Mention.Length;
            }
            return msg.Channel.Type == ChannelType.Private ? 0 : -1;
        }

        private static Task ClientReady(DiscordClient c, ReadyEventArgs e)
        {
            c.Logger.LogInformation(Astra, "Ready!");
            c.Logger.LogInformation(Astra,
                $"Your invite Link: https://discordapp.com/oauth2/authorize?client_id={c.CurrentApplication.Id}&scope=bot&permissions={Enum.GetValues<Permissions>().Aggregate(0L, (i, permissions) => i + (long) permissions)}");
#if !NO_CLI
            c.Logger.LogInformation(Astra, "Enter \"x\" to stop");
#endif
            c.UpdateStatusAsync(new DiscordActivity("help", ActivityType.ListeningTo));
            return Task.CompletedTask;
        }

        private static async Task MessageCreated(DiscordClient c, MessageCreateEventArgs e)
        {
            if (e.Guild != null && !e.Author.IsBot)
            {
                (await e.Guild.GetMemberAsync(e.Author.Id))
                    .IncrementMoney(Rnd.Next(0, Math.Max(e.Message.Content.Length / 25, 20)));
                int prx = await ResolvePrefix(e.Message);
                c.Logger.Log(LogLevel.Information, Astra, prx.ToString());
            }
            await c.UpdateStatusAsync(new DiscordActivity("help", ActivityType.ListeningTo));
        }

        private static async Task<bool> ExecCustomCommand(CommandContext context, ILogger<BaseDiscordClient> logger)
        {
            int prx = await ResolvePrefix(context.Message);
            if (prx >= 0)
            {
                string content = context.Message.Content.Substring(prx);
                List<string> param = new();
                char? quotes = null;
                param.Add("");
                foreach (char c1 in content)
                    if (c1 != '"' && c1 != '\'' && (c1 != ' ' || quotes != null))
                        param[^1] += c1;
                    else if (c1 == ' ')
                        param.Add("");
                    else if (c1 == '"')
                    {
                        if (quotes == '"')
                            quotes = null;
                        else if (quotes == null)
                            quotes = '"';
                        else
                            param[^1] += c1;
                    }
                    else if (c1 == '\'')
                    {
                        if (quotes == '\'')
                            quotes = null;
                        else if (quotes == null)
                            quotes = '\'';
                        else
                            param[^1] += c1;
                    }
                param = param.Where(s => !string.IsNullOrWhiteSpace(s)).ToList();
                GuildConfig gc = GuildConfig.Get(context.Guild);
                if (gc.CustomCommands.ContainsKey(param[0]))
                {
                    string command = gc.CustomCommands[param[0]];
                    CustomCommands.CCContext ctx = new(context.Member);
                    foreach (CustomCommands.Param param1 in CustomCommands.Params)
                        command = command.Replace($"{{{{{param1.Name}}}}}", param1.StringSelector(ctx));
                    command = command.Replace("{{*}}", content);
                    for (int i = 0; i < param.Count; i++)
                        command = command.Replace($"{{{{{i - 1}}}}}", param[i]);
                    DiscordEmbed embed = new DiscordEmbedBuilder
                    {
                        Author = new DiscordEmbedBuilder.EmbedAuthor
                        {
                            IconUrl = context.Member.AvatarUrl,
                            Name = context.Member.DisplayName
                        },
                        Description = command
                    }.Build();
                    await context.RespondAsync(embed);
                    logger.LogInformation(Astra, "Executed custom command: " + param[0]);
                    logger.LogInformation(Astra, command);
                    return true;
                }
            }
            return false;
        }

        private static Task ClientClientErrored(DiscordClient c, ClientErrorEventArgs e)
        {
            c.Logger.LogError(Astra, e.Exception, $"Exception in {e.EventName}");
            return Task.CompletedTask;
        }

        private static async Task Commands_CommandErrored(CommandsNextExtension n, CommandErrorEventArgs e)
        {
            if (e.Context.Channel.AstraEnabled())
            {
                if (e.Exception is UnwantedExecutionException)
                    return;
                // Custom commands
                if (await ExecCustomCommand(e.Context, n.Client.Logger))
                {
                    // Error handler
                    n.Client.Logger.LogError(Astra,
                        $"{e.Context.User.Username} tried executing '{e.Command?.QualifiedName ?? "<unknown command>"}' but it errored: {e.Exception}");
                    if (e.Exception is ChecksFailedException)
                        await e.Context.RespondAsync(new DiscordEmbedBuilder
                        {
                            Title = "Access denied",
                            Description =
                                $"{DiscordEmoji.FromName(n.Client, ":no_entry:")} You do not have the permissions required to execute this command.",
                            Color = new DiscordColor(0xFF0000)
                        }.Build());
                    else if (!(e.Exception is CommandNotFoundException))
                        await e.Context.RespondAsyncFix("The command failed: " +
#if VERBOSE
                                                        e.Exception
#else
                                                        e.Exception.Message
#endif
                        );
                }
            }
        }

        private static Task Commands_CommandExecuted(CommandsNextExtension n, CommandExecutionEventArgs e)
        {
            n.Client.Logger.LogInformation(Astra,
                $"{e.Context.User.Username} successfully executed '{e.Command.QualifiedName}'");
            return Task.CompletedTask;
        }
    }
}