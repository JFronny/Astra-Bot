using System;
using System.IO;
using Newtonsoft.Json;

namespace Bot.Config
{
    public class TokenData
    {
        private static readonly string ContainerFile = Path.Combine(PathResolver.BasePath, "tokens.json");
        public string DiscordToken { get; set; } = "";
        public string CurrConvToken { get; set; } = "";
        public string PerspectiveToken { get; set; } = "";

        public static TokenData Get()
        {
            ChkF();
            if (!File.Exists(ContainerFile))
            {
#if !NO_CLI
                TokenData td = new();
                Console.Write("Discord: ");
                td.DiscordToken = Console.ReadLine() ?? "";
                Console.Write("CurrConv: ");
                td.CurrConvToken = Console.ReadLine() ?? "";
                Console.Write("Perspective: ");
                td.PerspectiveToken = Console.ReadLine() ?? "";
                return td.Save();
#else
                Console.WriteLine($"Please set up a config file at {ContainerFile}");
                throw new System.IO.FileNotFoundException($"Please set up a config file at {ContainerFile}");
#endif
            }
            return JsonConvert.DeserializeObject<TokenData>(File.ReadAllText(ContainerFile));
        }

        private TokenData Save()
        {
            ChkF();
            File.WriteAllText(ContainerFile, JsonConvert.SerializeObject(this));
            return this;
        }

        private static void ChkF()
        {
            if (!Directory.Exists(Path.GetDirectoryName(ContainerFile)))
                Directory.CreateDirectory(Path.GetDirectoryName(ContainerFile));
        }
    }
}