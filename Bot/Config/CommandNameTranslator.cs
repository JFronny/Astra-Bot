using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

namespace Bot.Config
{
    public static class CommandNameTranslator
    {
        private static readonly IEnumerable<Group>? Groups;

        static CommandNameTranslator()
        {
            Groups = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(s => typeof(BaseCommandModule).IsAssignableFrom(s))
                .Select(s => new Group(s));
        }

        public static string GetConfigName(string commandName) =>
            Groups.SelectMany(s => s.Commands).First(s => s.CommandName == commandName).ConfigName;

        public static string GetCommandName(string configName) =>
            Groups.SelectMany(s => s.Commands).First(s => s.ConfigName == configName).CommandName;

        public static IEnumerable<string> GetConfigNames() =>
            Groups.SelectMany(s => s.Commands).Select(s => s.ConfigName);

        public static IEnumerable<string> GetCommandNames() =>
            Groups.SelectMany(s => s.Commands).Select(s => s.CommandName);

        public static IEnumerable<string> GetGroupNames() =>
            Groups.Select(s => s.Name);

        public static IEnumerable<string> GetContainedCommandNames(string group) =>
            Groups.First(s => s.Name == group).Commands.Select(s => s.CommandName);

        private class Group
        {
            public readonly IEnumerable<Command> Commands;
            public readonly string Name;

            public Group(Type type)
            {
                Name = type.GetCustomAttribute<GroupAttribute>().Name;
                Commands = type.GetMethods()
                    .Where(s => s.GetCustomAttributes(typeof(CommandAttribute), false).Length > 0)
                    .Select(s => new Command(s))
                    .GroupBy(s => s.CommandName)
                    .Select(s => s.First())
                    .Where(s => !new[] {"ping", "config"}.Contains(s.CommandName))
                    .OrderBy(s => s.CommandName);
            }
        }

        private class Command
        {
            public readonly string CommandName;
            public readonly string ConfigName;

            public Command(MethodInfo method)
            {
                ConfigName = method.Name;
                CommandName = method.GetCustomAttribute<CommandAttribute>().Name;
            }
        }
    }
}