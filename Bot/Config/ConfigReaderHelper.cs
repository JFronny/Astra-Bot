using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bot.Config.Data;
using DSharpPlus;
using DSharpPlus.Entities;

namespace Bot.Config
{
    public static class ConfigReaderHelper
    {
        public static string GetPrefix(this SnowflakeObject o) =>
            GetOverrides(o, ov => ov.Prefix)
            ?? throw new ArgumentException("Common prefix is null. Please specify a prefix!");

        public static void SetPrefix(this SnowflakeObject o, string value)
        {
            if (o is DiscordChannel c)
            {
                ChannelConfig cfg = ChannelConfig.Get(c);
                cfg.Overrides.Prefix =
                    GetPrefix(c.Guild).Equals(value, StringComparison.InvariantCultureIgnoreCase)
                        ? null
                        : value;
                cfg.Save();
            }
            else if (o is DiscordGuild g)
            {
                GuildConfig cfg = GuildConfig.Get(g);
                cfg.Overrides.Prefix =
                    CommonConfig.Get().Overrides.Prefix.Equals(value, StringComparison.InvariantCultureIgnoreCase)
                        ? null
                        : value;
                cfg.Save();
            }
            else
                throw new ArgumentException("Snowflake must be channel or guild", nameof(o));
        }

        public static bool AstraEnabled(this SnowflakeObject o)
        {
            if (o is DiscordChannel c)
                return ChannelConfig.Get(c).Overrides.BotEnabled;
            return true;
        }

        public static void AstraEnabled(this SnowflakeObject o, bool value)
        {
            if (o is DiscordChannel c)
            {
                ChannelConfig cfg = ChannelConfig.Get(c);
                cfg.Overrides.BotEnabled = value;
                cfg.Save();
            }
        }

        public static void IncrementMoney(this DiscordMember member, long amount)
        {
            GuildConfig c = GuildConfig.Get(member.Guild);
            if (!c.UserScores.ContainsKey(member.Id))
                c.UserScores.Add(member.Id, 0);
            c.UserScores[member.Id] += amount;
            c.Save();
        }

        public static long GetMoney(this DiscordMember member)
        {
            GuildConfig c = GuildConfig.Get(member.Guild);
            if (!c.UserScores.ContainsKey(member.Id))
                c.UserScores.Add(member.Id, 0);
            return c.UserScores[member.Id];
        }

        public static void SetMoney(this DiscordMember member, long amount)
        {
            GuildConfig c = GuildConfig.Get(member.Guild);
            if (!c.UserScores.ContainsKey(member.Id))
                c.UserScores.Add(member.Id, amount);
            c.UserScores[member.Id] = amount;
            c.Save();
        }

#if !NO_NSFW
        public static bool GetEvaluatedNsfw(this DiscordChannel channel) =>
            GetOverrides(channel, ov => ov.Nsfw)
            ?? channel.IsNSFW;
#endif

        public static bool MethodEnabled(this SnowflakeObject id, string method)
        {
            if (!id.AstraEnabled())
                return false;
            bool? v = GetOverrides(id, ov => ov.Command.GetValueOrDefault(method, null));
            if (v.HasValue)
                return v.Value;
            CommonConfig c = CommonConfig.Get();
            c.Overrides.Command.Add(method, true);
            c.Save();
            return true;
        }

        public static void MethodEnabled(this SnowflakeObject o, string method, bool value)
        {
            if (o is DiscordChannel c)
            {
                ChannelConfig cfg = ChannelConfig.Get(c);
                if (MethodEnabled(c.Guild, method) == value)
                    cfg.Overrides.Command.Remove(method);
                else
                {
                    if (!cfg.Overrides.Command.ContainsKey(method))
                        cfg.Overrides.Command.Add(method, value);
                    cfg.Overrides.Command[method] = value;
                }
                cfg.Save();
            }
            else if (o is DiscordGuild g)
            {
                GuildConfig cfg = GuildConfig.Get(g);
                if (CommonConfig.Get().Overrides.Command.GetValueOrDefault(method, !value) == value)
                    cfg.Overrides.Command.Remove(method);
                else
                {
                    if (!cfg.Overrides.Command.ContainsKey(method))
                        cfg.Overrides.Command.Add(method, value);
                    cfg.Overrides.Command[method] = value;
                }
                cfg.Save();
            }
            else
                throw new ArgumentException("Snowflake must be channel or guild", nameof(o));
        }

        public static void Clean(this ReactionRoles roles,
            Func<Dictionary<string, ulong>, IEnumerable<KeyValuePair<string, ulong>>> f)
        {
            IEnumerable<KeyValuePair<string, ulong>> vs = f(roles.Roles);
            roles.Roles.Clear();
            foreach (KeyValuePair<string, ulong> kvp in vs) roles.Roles.Add(kvp.Key, kvp.Value);
        }

        public static async Task<DiscordMessage> Get(this MessageRef message, DiscordClient client) =>
            await (await client.GetChannelAsync(message.ChannelId)).GetMessageAsync(message.MessageId);

        private static TRes? GetOverrides<TRes>(SnowflakeObject source, Func<ConfigOverrides, TRes?> selector)
        {
            if (source is DiscordChannel c)
                return selector(ChannelConfig.Get(c).Overrides)
                       ?? selector(GuildConfig.Get(c.Guild).Overrides)
                       ?? selector(CommonConfig.Get().Overrides);
            if (source is DiscordGuild g)
                return selector(GuildConfig.Get(g).Overrides)
                       ?? selector(CommonConfig.Get().Overrides);
            throw new ArgumentException("Snowflake must be channel or guild", nameof(source));
        }

#if !NO_TIMED_BAN
        public static void BanTimed(this DiscordMember member, TimeSpan time)
        {
            GuildConfig c = GuildConfig.Get(member.Guild);
            c.TimedBans.Add(member.Id, DateTime.Now + time);
            c.Save();
        }

        public static void RemoveTimeBan(this DiscordGuild guild, ulong id)
        {
            GuildConfig c = GuildConfig.Get(guild);
            if (c.TimedBans.ContainsKey(id))
            {
                c.TimedBans.Remove(id);
                c.Save();
            }
        }

        public static bool IsTimeBanned(this DiscordGuild guild, ulong id)
        {
            GuildConfig c = GuildConfig.Get(guild);
            return c.TimedBans.ContainsKey(id);
        }

        public static TimeSpan GetBanTime(this DiscordGuild guild, ulong id)
        {
            GuildConfig c = GuildConfig.Get(guild);
            return c.TimedBans[id] - DateTime.Now;
        }
#endif
    }
}