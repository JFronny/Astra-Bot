using System;
using System.Collections.Generic;
using System.IO;
using DSharpPlus.Entities;
using static Bot.Config.PathResolver;

namespace Bot.Config.Data
{
    public class GuildConfig
    {
        private static readonly Dictionary<ulong, GuildConfig> Configs = new();
        private string _customCommandsPath;

        private string _overridesPath;
        private string _reactionRolesPath;
        private string _timedBansPath;
        private string _userScoresPath;
        public Dictionary<string, string> CustomCommands;

        public ConfigOverrides Overrides;
        public ReactionRoles ReactionRoles;
        public Dictionary<ulong, DateTime> TimedBans;
        public Dictionary<ulong, long> UserScores;

        public static GuildConfig Get(DiscordGuild guild)
        {
            if (!Configs.ContainsKey(guild.Id))
            {
                string op = Path.Combine(GetGuildPath(guild.Id), "overrides.json");
                string tbp = Path.Combine(GetGuildPath(guild.Id), "timedBans.json");
                string usp = Path.Combine(GetGuildPath(guild.Id), "userScores.json");
                string rrp = Path.Combine(GetGuildPath(guild.Id), "reactionRoles.json");
                string ccp = Path.Combine(GetGuildPath(guild.Id), "customCommands.json");
                Configs.Add(guild.Id, new GuildConfig
                {
                    _overridesPath = op,
                    _timedBansPath = tbp,
                    _userScoresPath = usp,
                    _reactionRolesPath = rrp,
                    _customCommandsPath = ccp,

                    Overrides = LoadOrDefault(op, new ConfigOverrides()),
                    TimedBans = LoadOrDefault(tbp, new Dictionary<ulong, DateTime>()),
                    UserScores = LoadOrDefault(usp, new Dictionary<ulong, long>()),
                    ReactionRoles = LoadOrDefault(rrp, new ReactionRoles()),
                    CustomCommands = LoadOrDefault(ccp, new Dictionary<string, string>())
                });
            }
            return Configs[guild.Id];
        }

        public void Save()
        {
            SaveJ(_overridesPath, Overrides);
            SaveJ(_timedBansPath, TimedBans);
            SaveJ(_userScoresPath, UserScores);
            SaveJ(_reactionRolesPath, ReactionRoles);
            SaveJ(_customCommandsPath, CustomCommands);
        }
    }
}