using System.IO;
using static Bot.Config.PathResolver;

namespace Bot.Config.Data
{
    public class CommonConfig
    {
        private static readonly CommonConfig Config = new(Path.Combine(BasePath, "defaults.json"));
        private readonly string _overridesPath;

        public readonly ConfigOverrides Overrides;

        private CommonConfig(string path)
        {
            _overridesPath = path;
            Overrides = LoadOrDefault(path, new ConfigOverrides());
            Overrides.Prefix ??= "!";
        }

        public static CommonConfig Get() => Config;
        public void Save() => SaveJ(_overridesPath, Overrides);
    }
}