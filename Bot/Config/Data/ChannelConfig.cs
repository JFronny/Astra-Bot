using System.Collections.Generic;
using System.IO;
using DSharpPlus.Entities;
using static Bot.Config.PathResolver;

namespace Bot.Config.Data
{
    public class ChannelConfig
    {
        private static readonly Dictionary<ulong, Dictionary<ulong, ChannelConfig>> Configs = new();

        private string _overridesPath;

        public ConfigOverridesChannel Overrides;

        public static ChannelConfig Get(DiscordChannel channel)
        {
            ulong guild = channel.GuildId.Value;
            if (!Configs.ContainsKey(guild))
                Configs.Add(guild, new Dictionary<ulong, ChannelConfig>());
            if (!Configs[guild].ContainsKey(channel.Id))
            {
                string p = Path.Combine(GetChannelPath(guild, channel.Id), "overrides.json");
                Configs[guild].Add(channel.Id, new ChannelConfig
                {
                    _overridesPath = p,
                    Overrides = LoadOrDefault(p, new ConfigOverridesChannel())
                });
            }
            return Configs[guild][channel.Id];
        }

        public void Save()
        {
            SaveJ(_overridesPath, Overrides);
        }
    }
}