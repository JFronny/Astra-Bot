namespace Bot.Config.Data
{
    public class MessageRef
    {
        public readonly ulong ChannelId;
        public readonly ulong MessageId;

        public MessageRef(ulong channelId, ulong messageId)
        {
            ChannelId = channelId;
            MessageId = messageId;
        }
    }
}