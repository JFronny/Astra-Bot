using System.Collections.Generic;

namespace Bot.Config.Data
{
    public class ConfigOverrides
    {
        public readonly Dictionary<string, bool?> Command = new();
        public bool? Nsfw = null;
        public string? Prefix = null;
    }
}