using System.Collections.Generic;

namespace Bot.Config.Data
{
    public class ReactionRoles
    {
        public readonly Dictionary<string, ulong> Roles = new();
        public MessageRef? Message;
    }
}