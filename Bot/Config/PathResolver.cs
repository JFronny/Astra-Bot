using System.IO;
using System.Reflection;
using Newtonsoft.Json;

namespace Bot.Config
{
    public static class PathResolver
    {
        public const string Guilds = "guilds";
        public const string Channels = "channels";

        public static readonly string BasePath = Path.Combine(
            Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? ".", "Cfgs");

        static PathResolver()
        {
            if (!Directory.Exists(BasePath))
                Directory.CreateDirectory(BasePath);
        }

        public static string GetGuildPath(ulong guildId) =>
            MkDir(Path.Combine(BasePath, Guilds, guildId.ToString()));

        public static string GetChannelPath(ulong guildId, ulong channelId) =>
            MkDir(Path.Combine(GetGuildPath(guildId), Channels, channelId.ToString()));

        private static string MkDir(string p)
        {
            if (!Directory.Exists(p))
                Directory.CreateDirectory(p);
            return p;
        }

        public static T LoadOrDefault<T>(string path, T def)
        {
            if (!File.Exists(path))
                SaveJ(path, def);
            return JsonConvert.DeserializeObject<T>(File.ReadAllText(path));
        }

        public static void SaveJ<T>(string path, T value)
        {
            File.WriteAllText(path, JsonConvert.SerializeObject(value));
        }
    }
}