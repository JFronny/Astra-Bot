using System.Collections.Generic;
using System.Linq;
using Bot.Config;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Converters;
using DSharpPlus.CommandsNext.Entities;
using DSharpPlus.Entities;
using Shared;

namespace Bot
{
    public class HelpFormatter : BaseHelpFormatter
    {
        private const string AutodocBase = "https://jfronny.gitlab.io/Astra-Bot/autodoc";
        private readonly DiscordEmbedBuilder _builder;
        private readonly CommandContext _ctx;
        private Command? _command;
        private string _doclink;

        public HelpFormatter(CommandContext ctx) : base(ctx)
        {
            _builder = new DiscordEmbedBuilder();
            _ctx = ctx;
        }

        public override BaseHelpFormatter WithCommand(Command command)
        {
            _command = command;
            bool group = command is CommandGroup;
            _builder.Title = $"{command.QualifiedName} ({(group ? "Group" : "Command")})";
            if (!_ctx.Channel.MethodEnabled(command.Name))
                _builder.Title += " (disabled)";
            if (command.Aliases.Any())
                _builder.AddField("Aliases", string.Join(", ", command.Aliases.Select(s => $"`{s}`")));
            if (command.Overloads.Any())
                _builder.AddField("Overloads", string.Join("\n\n",
                    command.Overloads.OrderBy(s => s.Priority).Select(s =>
                    {
                        return
                            $"{command.QualifiedName}{(s.Arguments.Any() ? "\n" : "")}{string.Join("\n", s.Arguments.Select(a => { string tmp = $"-   `{a.Name} "; string type = CommandsNext.GetUserFriendlyTypeName(a.Type); if (a.IsCatchAll) tmp += $"[{type}...]"; else if (a.IsOptional) tmp += $"({type})"; else tmp += $"<{type}>"; return $"{tmp}`: {a.Description}"; }))}";
                    })
                ));
            _doclink =
                $"{AutodocBase}/{(group ? $"{command.Name}" : $"{command.Parent.Name}/{command.Name}")}";
            _builder.Description = command.Description;
            return this;
        }

        public override BaseHelpFormatter WithSubcommands(IEnumerable<Command> subCommands)
        {
            string text = string.Join("\n", subCommands
                .Where(s => !s.IsHidden)
                .Select(s => s is CommandGroup group
                    ? $"{s.Name}: {string.Join(" ", group.Children.Where(a => !a.IsHidden).Distinct(new CommandComparer()).Select(a => $"`{a.Name}`"))}"
                    : $"`{s.Name}`")
            );
            _builder.AddField("Commands", text);
            return this;
        }

        public override CommandHelpMessage Build()
        {
            if (!_ctx.Channel.AstraEnabled()) throw new UnwantedExecutionException();
            if (_command == null)
                _builder.AddField("Notes",
                        "You can use \"help [group] [command]\" to get help about a specific command or \"help [group]\" for a group")
                    .AddField($"Current Prefix (`{_ctx.Client.CurrentUser.Mention} a c Prefix` to configure)",
                        _ctx.Channel.GetPrefix())
                    .WithTitle("Help");
            _builder.AddField("Autodoc",
                string.IsNullOrWhiteSpace(_doclink) ? AutodocBase : _doclink);
            return new CommandHelpMessage(embed: _builder.Build());
        }
    }
}