using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Bot.Config;
using Bot.Config.Data;
using DSharpPlus;
using DSharpPlus.Entities;
using Microsoft.Extensions.Logging;

namespace Bot
{
    public class ProcessLoop
    {
        private readonly DiscordClient _client;

        public ProcessLoop(DiscordClient client) => _client = client;

        public async Task<bool> RunIteration()
        {
            try
            {
#if !NO_CLI
                if (Console.KeyAvailable && Console.Read() == 'x') return true;
#endif
                Thread.Sleep(5000);
#if !NO_TIMED_BAN
                foreach (DiscordGuild guild in _client.Guilds.Values)
                {
                    GuildConfig gc = GuildConfig.Get(guild);
                    foreach (KeyValuePair<ulong, DateTime> ban in gc.TimedBans.ToArray())
                        if (guild.Members.ContainsKey(ban.Key))
                            gc.TimedBans.Remove(ban.Key);
                        else if (ban.Value < DateTime.Now)
                        {
                            await guild.UnbanMemberAsync(ban.Key, "Timed ban ran out");
                            gc.TimedBans.Remove(ban.Key);
                        }
                    gc.Save();
                }
#endif
            }
            catch (Exception e)
            {
                _client.Logger.LogError(Program.Astra, e, "A crash occured in the main Thread");
            }
            finally
            {
                try
                {
                    await EvaluateRr();
                }
                catch (Exception e)
                {
                    _client.Logger.LogError(Program.Astra, e, "A crash occured in the reaction-roles Thread");
                }
            }
            return false;
        }

        private async Task EvaluateRr()
        {
            foreach (DiscordGuild guild in _client.Guilds.Values)
            {
                GuildConfig gc = GuildConfig.Get(guild);
                gc.ReactionRoles.Clean(roles => roles.Where(s => guild.Roles.ContainsKey(s.Value)));
                gc.ReactionRoles.Clean(roles => roles.GroupBy(s => s.Value)
                    .Select(s => s.First()));
                gc.ReactionRoles.Clean(roles => roles.Take(20));
                MessageRef? message = gc.ReactionRoles.Message;
                if (message == null) continue;
                DiscordMessage msg = await message.Get(_client);
                foreach (DiscordEmoji disallowed in msg.Reactions.Where(s => !s.IsMe).Select(s => s.Emoji))
                    await msg.DeleteReactionsEmojiAsync(disallowed);
                foreach (KeyValuePair<string, ulong> role in gc.ReactionRoles.Roles)
                {
                    DiscordEmoji emoji = DiscordEmoji.FromName(_client, role.Key);
                    if (!msg.Reactions.Any(s => s.IsMe && s.Emoji.GetDiscordName() == role.Key))
                        await msg.CreateReactionAsync(emoji);
                    IReadOnlyList<DiscordUser> reactions = msg.GetReactionsAsync(emoji).Result;
                    DiscordRole dRole = guild.GetRole(role.Value);
                    foreach (KeyValuePair<ulong, DiscordMember> member in guild.Members)
                    {
                        bool inGroup = member.Value.Roles.Contains(dRole);
                        bool shouldBeInGroup = reactions.Contains(member.Value);
                        if (inGroup == shouldBeInGroup) continue;
                        if (shouldBeInGroup)
                            await member.Value.GrantRoleAsync(dRole);
                        else
                            await member.Value.RevokeRoleAsync(dRole);
                    }
                }
                gc.Save();
            }
        }
    }
}