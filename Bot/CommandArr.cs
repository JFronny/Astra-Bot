/*namespace Bot
{
    public static class CommandArr
    {
        private static IEnumerable<Group>? _groups;
        private static IEnumerable<string>? _commandNames;

        private static IEnumerable<Group> GetGroups() => _groups ??= Assembly.GetExecutingAssembly()
            .GetTypes().Where(s => typeof(BaseCommandModule).IsAssignableFrom(s)).Select(s => new Group(s,
                $"group_{s.GetCustomAttributes<GroupAttribute>().First().Name}"));

        private static IEnumerable<string> GetCommandNames(Type group) =>
            group.GetMethods().Where(s => s.GetCustomAttributes(typeof(CommandAttribute), false).Length > 0)
                .Select(CommandComparer.GetName)
                .Except(new[] {"Ping", "ConfigCmd"})
                .OrderBy(s => s);

        public static IEnumerable<string> GetGroupNames() => GetGroups().Select(s => s.Name);

        public static IEnumerable<string> GetCommandNames(string group) => GetCommandNames(
            GetGroups().First(s => string.Equals(s.Name, group, StringComparison.CurrentCultureIgnoreCase)).Type);

        public static string[] GetCommandNames() =>
            (_commandNames ??= GetGroups().Select(s => s.Type).SelectMany(GetCommandNames)).ToArray();

        private class Group
        {
            public Type Type;
            public string Name;

            public Group(Type type, string name)
            {
                Type = type;
                Name = name;
            }
        }
    }
}*/

