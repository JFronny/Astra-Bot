using System.Reflection;
using System.Threading.Tasks;
using Bot.Commands.Misc;
using Bot.Converters;
using DSharpPlus.CommandsNext;

namespace Bot
{
    public static class Register
    {
        public static async Task RegisterAll(this CommandsNextExtension client)
        {
            await Quotes.Initialize();
            client.RegisterCommands(Assembly.GetExecutingAssembly());
            client.RegisterConverter(new BoardConv());
            client.RegisterConverter(new BooruConv());
            client.RegisterConverter(new CurrencyConv());
            client.RegisterConverter(new DiscordNamedColorConverter());
            client.RegisterConverter(new EnumConv<DoujinSources>());
            client.RegisterConverter(new EnumConv<HashType>());
            client.RegisterConverter(new EnumConv<RpsOption>());
            client.SetHelpFormatter<HelpFormatter>();
        }
    }
}