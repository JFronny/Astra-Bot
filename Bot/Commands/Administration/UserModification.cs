using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Bot.Config;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Shared;

namespace Bot.Commands.Administration
{
    public partial class Administration
    {
        [Command("ban")]
        [Aliases("b")]
        [RequirePermissions(Permissions.BanMembers)]
        [Description("Bans the selected user")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Ban(CommandContext ctx, DiscordMember member,
            [Description("Reason for the ban")] [RemainingText]
            string reason)
        {
            if (ctx.Channel.MethodEnabled(nameof(Ban)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                await member.BanAsync(reason: reason);
                await ctx.RespondAsync($"Banned {member.DisplayName}.");
            }
        }

#if !NO_TIMED_BAN
        [Command("ban")]
        [RequirePermissions(Permissions.BanMembers)]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Ban(CommandContext ctx, DiscordMember member,
            [Description("Period of time to ban the member for")]
            TimeSpan time, [Description("Reason for the ban")] [RemainingText]
            string reason)
        {
            if (ctx.Channel.MethodEnabled(nameof(Ban)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                await ctx.Guild.BanMemberAsync(member, reason: $"{reason}\nBanned until {DateTime.Now + time:g}");
                member.BanTimed(time);
                await ctx.RespondAsync($"Banned {member.DisplayName}.");
            }
        }
#endif

        [Command("unban")]
        [Aliases("u", "ub", "uban", "deban", "rmban")]
        [RequirePermissions(Permissions.BanMembers)]
        [Description("Unbans the selected user")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Unban(CommandContext ctx, DiscordUser user,
            [Description("Reason for the unban")] [RemainingText]
            string reason)
        {
            if (ctx.Channel.MethodEnabled(nameof(Unban)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                await ctx.Guild.UnbanMemberAsync(user, reason);
#if !NO_TIMED_BAN
                ctx.Guild.RemoveTimeBan(user.Id);
#endif
                await ctx.RespondAsync($"Unbanned {user.Username}.");
            }
        }

        [Command("bans")]
        [Aliases("lb", "listbans", "lsb", "lsbans")]
        [RequirePermissions(Permissions.BanMembers)]
        [Description("Lists all banned users")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Bans(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(Bans)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
#if NO_TIMED_BAN
                IEnumerable<string> bans = (await ctx.Guild.GetBansAsync())
                    .Select(s =>
                        $"User: {s.User.Username}\nReason: {s.Reason}");
#else
                IEnumerable<string> bans = (await ctx.Guild.GetBansAsync())
                    .Select(s =>
                        $"User: {s.User.Username}\nReason: {s.Reason}\nTimed: {(ctx.Guild.IsTimeBanned(s.User.Id) ? $"Yes, {ctx.Guild.GetBanTime(s.User.Id):g} left" : "No")}");
#endif
                await ctx.RespondPaginatedIfTooLong($"Banned Users:\n{string.Join("\n\n", bans)}");
            }
        }

        [Command("softban")]
        [Aliases("sb", "sban")]
        [RequirePermissions(Permissions.BanMembers)]
        [Description("Kicks the member and deletes their messages")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Softban(CommandContext ctx, [Description("Member to softban")] DiscordMember member,
            [Description("Reason for the softban")] [RemainingText]
            string reason)
        {
            if (ctx.Channel.MethodEnabled(nameof(Softban)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                await member.BanAsync(7, reason);
                await member.UnbanAsync();
                await ctx.RespondAsync($"Softbanned {member.Username}.");
            }
        }

        [Command("kick")]
        [Aliases("k")]
        [RequirePermissions(Permissions.KickMembers)]
        [Description("Kicks the member")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Kick(CommandContext ctx, [Description("Member to softban")] DiscordMember member,
            [Description("Reason for the softban")] [RemainingText]
            string reason)
        {
            if (ctx.Channel.MethodEnabled(nameof(Kick)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                await member.RemoveAsync(reason);
                await ctx.RespondAsync($"Kicked {member.Username}.");
            }
        }

        [Command("nick")]
        [Aliases("n")]
        [Description("Gives the member a new nickname")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Nick(CommandContext ctx, [Description("New nickname")] string nickname,
            [Description("Member to softban")] DiscordMember? member = null)
        {
            if (ctx.Channel.MethodEnabled(nameof(Nick)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                if (member == null)
                    member = ctx.Member;
                Permissions userPer = ctx.Member.PermissionsIn(ctx.Channel);
                if (member.Id == ctx.Member.Id && userPer.HasPermission(Permissions.ChangeNickname) ||
                    userPer.HasPermission(Permissions.ManageNicknames))
                {
                    await member.ModifyAsync(s => s.Nickname = nickname);
                    await ctx.RespondAsync($"Set the nickname of {member.Username} to {nickname}.");
                }
                else
                    throw new Exception("I cannot allow you to do that.");
            }
        }

        [Command("mute")]
        [Aliases("m", "silent", "quiet")]
        [RequirePermissions(Permissions.MuteMembers)]
        [Description("(Un)Mutes the member")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Mute(CommandContext ctx, [Description("Member to (Un)Mute")] DiscordMember member,
            [Description("Set to \"true\" to mute, false to undo")]
            bool mute,
            [Description("Reason for the (Un)Mute")] [RemainingText]
            string reason)
        {
            if (ctx.Channel.MethodEnabled(nameof(Mute)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                await member.SetMuteAsync(mute, reason);
                await ctx.RespondAsync($"{(mute ? "Muted" : "Unmuted")} {member.Username}.");
            }
        }

        [Command("deaf")]
        [Aliases("d")]
        [RequirePermissions(Permissions.DeafenMembers)]
        [Description("(Un)Deafen the member")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Deaf(CommandContext ctx, [Description("Member to (Un)Deafen")] DiscordMember member,
            [Description("Set to \"true\" to deafen, false to undo")]
            bool deafen,
            [Description("Reason for the (Un)Deafen")] [RemainingText]
            string reason)
        {
            if (ctx.Channel.MethodEnabled(nameof(Deaf)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                await member.SetDeafAsync(deafen, reason);
                await ctx.RespondAsync($"{(deafen ? "Deafened" : "Undeafened")} {member.Username}.");
            }
        }
    }
}