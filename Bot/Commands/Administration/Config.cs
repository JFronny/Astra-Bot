using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Bot.Config;
using Bot.Config.Data;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using Shared;

namespace Bot.Commands.Administration
{
    public partial class Administration
    {
        private const string Enabled = "enabled";
        private const string Prefix = "prefix";

        [Command("config")]
        [Aliases("cfg", "c")]
        [RequirePermissions(Permissions.ManageGuild)]
        [Description(
            "Prints or changes the config for this channel/guild (empty for guild). You can also set all commands in a group by using \"group_[NAME]\"")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Task ConfigCmd(CommandContext ctx) => ConfigCmd(ctx, ctx.Guild);

        [Command("reset-config")]
        [RequirePermissions(Permissions.ManageGuild)]
        [Description("Reverts all configs")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task ConfigReset(CommandContext ctx)
        {
            await ctx.RequireGuild();
            InteractivityExtension interactivity = ctx.Client.GetInteractivity();
            DiscordMessage msg =
                await ctx.RespondAsync(
                    "Please type out \"yes\" to confirm. All configuration for your server will be reset!");
            InteractivityResult<DiscordMessage> tmp =
                await interactivity.WaitForMessageAsync(s => s.Author == ctx.Message.Author, new TimeSpan(0, 0, 30));
            if (!tmp.TimedOut || tmp.Result.Content != "yes")
            {
                foreach (DiscordChannel channel in ctx.Guild.Channels.Values)
                    ResetChannel(channel);
                GuildConfig cfg = GuildConfig.Get(ctx.Guild);
                ResetOverrides(cfg.Overrides);
                cfg.ReactionRoles.Message = null;
                cfg.ReactionRoles.Roles.Clear();
                cfg.TimedBans.Clear();
                cfg.UserScores.Clear();
                cfg.Save();
                await msg.ModifyAsync("Reset completed.");
            }
            else
                await msg.ModifyAsync("Cancelled.");
        }

        [Command("reset-config")]
        [RequirePermissions(Permissions.ManageGuild)]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task ConfigReset(CommandContext ctx, [Description("The channel to reset")] DiscordChannel channel)
        {
            await ctx.RequireGuild();
            InteractivityExtension interactivity = ctx.Client.GetInteractivity();
            DiscordMessage msg =
                await ctx.RespondAsync(
                    "Please type out \"yes\" to confirm. All configuration for this channel will be reset!");
            InteractivityResult<DiscordMessage> tmp =
                await interactivity.WaitForMessageAsync(s => s.Author == ctx.Message.Author, new TimeSpan(0, 0, 30));
            if (!tmp.TimedOut || tmp.Result.Content != "yes")
            {
                ResetChannel(channel);
                await msg.ModifyAsync("Reset completed.");
            }
            else
                await msg.ModifyAsync("Cancelled.");
        }

        private void ResetChannel(DiscordChannel channel)
        {
            ChannelConfig cfg = ChannelConfig.Get(channel);
            cfg.Overrides.BotEnabled = true;
            ResetOverrides(cfg.Overrides);
            cfg.Save();
        }

        private void ResetOverrides(ConfigOverrides overrides)
        {
            overrides.Command.Clear();
            overrides.Nsfw = null;
            overrides.Prefix = null;
        }

        [Command("config")]
        [RequirePermissions(Permissions.ManageGuild)]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Task ConfigCmd(CommandContext ctx, [Description("The channel to configure")]
            DiscordChannel channel) => ConfigCmd(ctx, (SnowflakeObject) channel);

        private async Task ConfigCmd(CommandContext ctx, SnowflakeObject target)
        {
            await ctx.RequireGuild();
            if (ctx.Channel.AstraEnabled())
            {
                await ctx.TriggerTypingAsync();
                StringBuilder sb = new();
                sb.Append("Options:");
                sb.Append($"\n{Enabled}: {target.AstraEnabled()}");
                sb.Append($"\n{Prefix}: {target.GetPrefix()}");
                foreach (string s in CommandNameTranslator.GetConfigNames())
                {
                    sb.Append("\n");
                    sb.Append(CommandNameTranslator.GetCommandName(s));
                    sb.Append(": ");
                    sb.Append(target.MethodEnabled(s));
                }
                await ctx.RespondAsync(sb.ToString());
            }
        }

        [Command("config")]
        [RequirePermissions(Permissions.ManageGuild)]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Task ConfigCmd(CommandContext ctx, [Description("Config element to print")]
            string element) => ConfigCmd(ctx, ctx.Guild, element);

        [Command("config")]
        [RequirePermissions(Permissions.ManageGuild)]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Task ConfigCmd(CommandContext ctx, [Description("The channel to configure")]
            DiscordChannel channel, [Description("Config element to print")]
            string element) => ConfigCmd(ctx, (SnowflakeObject) channel, element);

        private async Task ConfigCmd(CommandContext ctx, SnowflakeObject target, string element)
        {
            await ctx.RequireGuild();
            if (ctx.Channel.AstraEnabled())
            {
                await ctx.TriggerTypingAsync();
                if (string.Equals(element, Prefix, StringComparison.InvariantCultureIgnoreCase))
                    await ctx.RespondAsync($"{Prefix}: {target.GetPrefix()}");
                else if (string.Equals(element, Enabled, StringComparison.InvariantCultureIgnoreCase))
                    await ctx.RespondAsync($"{Enabled}: {target.AstraEnabled()}");
                else
                {
                    if (!CommandNameTranslator.GetCommandNames().Contains(element))
                        throw new ArgumentException($"Element ({element}) not a command");
                    await ctx.RespondAsync(
                        $"{element}: {target.MethodEnabled(CommandNameTranslator.GetConfigName(element))}");
                }
            }
        }

        [Command("config")]
        [RequirePermissions(Permissions.ManageGuild)]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Task ConfigCmd(CommandContext ctx, [Description("Config element to change")]
            string element, [Description("New value")] string value) =>
            ConfigCmd(ctx, ctx.Guild, element, value);

        [Command("config")]
        [RequirePermissions(Permissions.ManageGuild)]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Task ConfigCmd(CommandContext ctx, [Description("The channel to configure")]
            DiscordChannel channel, [Description("Config element to change")]
            string element, [Description("New value")] string value) =>
            ConfigCmd(ctx, (SnowflakeObject) channel, element, value);

        private async Task ConfigCmd(CommandContext ctx, SnowflakeObject target, string element, string value,
            bool noComment = false)
        {
            if (ctx.Channel.AstraEnabled()
                || element == Enabled
                || ctx.Guild.Channels.Values.All(s => !s.AstraEnabled()))
            {
                await ctx.RequireGuild();
                if (!noComment)
                    await ctx.TriggerTypingAsync();
                if (target is DiscordGuild guild)
                {
                    if (string.Equals(element, Enabled, StringComparison.CurrentCultureIgnoreCase))
                    {
                        await ctx.RespondAsync("Please do not EVER set \"enabled\" to false globally!");
                        return;
                    }
                    foreach ((ulong _, DiscordChannel channel) in guild.Channels)
                        await ConfigCmd(ctx, channel, element, value, true);
                }
                if (string.Equals(element, Prefix, StringComparison.CurrentCultureIgnoreCase))
                    target.SetPrefix(value);
                else if (string.Equals(element, Enabled, StringComparison.InvariantCultureIgnoreCase))
                    target.AstraEnabled(bool.Parse(value));
                else if (CommandNameTranslator.GetGroupNames().Contains(element))
                    foreach (string t in CommandNameTranslator.GetContainedCommandNames(element))
                        await ConfigCmd(ctx, target, t, value, true);
                else if (!CommandNameTranslator.GetCommandNames().Contains(element))
                    throw new ArgumentException($"Element ({element}) not in CommandArr");
                else
                    target.MethodEnabled(CommandNameTranslator.GetConfigName(element), bool.Parse(value));
                if (!noComment)
                    await ctx.RespondAsync($"Set {element} to {value}");
            }
        }
    }
}