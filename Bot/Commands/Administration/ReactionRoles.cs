using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Bot.Config;
using Bot.Config.Data;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Shared;

namespace Bot.Commands.Administration
{
    [Group("reactionroles")]
    [Aliases("rr")]
    [Description("ReactionRoles allows users to give themselves roles using reactions on a message")]
    public class ReactionRoles : BaseCommandModule
    {
        [Command("bind")]
        [RequirePermissions(Permissions.ManageRoles)]
        [Description("Bind ReactionRoles to a new guild-wide message")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Bind(CommandContext ctx, [Description("Channel to post in, defaults to current")]
            DiscordChannel? channel = null)
        {
            if (ctx.Channel.MethodEnabled(nameof(Bind)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                channel ??= ctx.Channel;
                await Bind(ctx, await channel.SendMessageAsync("Please react to this message to get your roles"));
            }
        }

        [Command("bind")]
        [RequirePermissions(Permissions.ManageRoles)]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Bind(CommandContext ctx, [Description("Message to bind")] DiscordMessage msg)
        {
            if (ctx.Channel.MethodEnabled(nameof(Bind)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                GuildConfig c = GuildConfig.Get(ctx.Guild);
                c.ReactionRoles.Message = new MessageRef(msg.ChannelId, msg.Id);
                c.Save();
                await ctx.RespondAsync("Successfully wrote key");
            }
        }

        [Command("unbind")]
        [RequirePermissions(Permissions.ManageRoles)]
        [Description("Unbind ReactionRoles")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Unbind(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(Unbind)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                GuildConfig c = GuildConfig.Get(ctx.Guild);
                MessageRef? r = c.ReactionRoles.Message;
                if (r == null)
                {
                    await ctx.RespondAsync("Already unbound");
                    return;
                }
                DiscordMessage msg = await r.Get(ctx.Client);
                if (msg.Author.IsCurrent)
                    await msg.DeleteAsync();
                c.ReactionRoles.Message = null;
                c.Save();
                await ctx.RespondAsync("Done.");
            }
        }

        [Command("list")]
        [Aliases("ls")]
        [Description("List all registered roles")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task List(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(List)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                Dictionary<string, ulong> roles = GuildConfig.Get(ctx.Guild).ReactionRoles.Roles;
                if (roles.Count == 0)
                    await ctx.RespondAsync("No items are bound");
                else
                {
                    string text = "";
                    foreach (KeyValuePair<string, ulong> pair in roles)
                        text += $"{pair.Key} - {ctx.Guild.GetRole(pair.Value).Name}\n";
                    await ctx.RespondPaginatedIfTooLong(text.Remove(text.Length - 1));
                }
            }
        }

        [Command("add")]
        [RequirePermissions(Permissions.ManageRoles)]
        [Aliases("create")]
        [Description("Binds a new role and emoji to RR")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Add(CommandContext ctx, [Description("The emoji to bind")] DiscordEmoji emoji,
            [Description("The role to bind")] DiscordRole role)
        {
            if (ctx.Channel.MethodEnabled(nameof(Add)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                GuildConfig c = GuildConfig.Get(ctx.Guild);
                if (c.ReactionRoles.Roles.Count > 19)
                {
                    await ctx.RespondAsync("Too many roles!");
                    return;
                }
                if (c.ReactionRoles.Roles.ContainsKey(emoji.Name) || c.ReactionRoles.Roles.ContainsValue(role.Id))
                {
                    await ctx.RespondAsync("Element already registered");
                    return;
                }
                Console.WriteLine(role.Name);
                //I know hard-coding @everyone is bad code but it works for now
                if (emoji.IsManaged || role.IsManaged || role.Name == "@everyone")
                {
                    await ctx.RespondAsync("Invalid value");
                    return;
                }
                c.ReactionRoles.Roles.Add(emoji.GetDiscordName(), role.Id);
                c.Save();
                await ctx.RespondAsync("Done!");
            }
        }

        [Command("remove")]
        [RequirePermissions(Permissions.ManageRoles)]
        [Aliases("rm", "del")]
        [Description("Unbinds a role from RR")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Remove(CommandContext ctx, [Description("The emoji to unbind")] DiscordEmoji emoji)
        {
            if (ctx.Channel.MethodEnabled(nameof(Remove)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                GuildConfig c = GuildConfig.Get(ctx.Guild);
                c.ReactionRoles.Roles.Remove(emoji.GetDiscordName());
                c.Save();
                await ctx.RespondAsync("Done!");
            }
        }

        [Command("remove")]
        [RequirePermissions(Permissions.ManageRoles)]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Remove(CommandContext ctx, [Description("The role to unbind")] DiscordRole role)
        {
            if (ctx.Channel.MethodEnabled(nameof(Remove)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                GuildConfig c = GuildConfig.Get(ctx.Guild);
                c.ReactionRoles.Roles.Remove(c.ReactionRoles.Roles.First(s => s.Value == role.Id).Key);
                c.Save();
                await ctx.RespondAsync("Done!");
            }
        }

        [Command("clear")]
        [RequirePermissions(Permissions.ManageRoles)]
        [Description("Unbinds all roles from RR")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Clear(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(Clear)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                GuildConfig c = GuildConfig.Get(ctx.Guild);
                c.ReactionRoles.Roles.Clear();
                c.Save();
                await ctx.RespondAsync("Done!");
            }
        }

        [Command("jumplink")]
        [RequirePermissions(Permissions.ManageRoles)]
        [Aliases("jump", "link")]
        [Description("Unbinds all roles from RR")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task JumpLink(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(JumpLink)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                MessageRef r = GuildConfig.Get(ctx.Guild).ReactionRoles.Message
                               ?? throw new Exception("No message is configured in your guild");
                await ctx.RespondAsync((await r.Get(ctx.Client)).JumpLink.AbsoluteUri);
            }
        }
    }
}