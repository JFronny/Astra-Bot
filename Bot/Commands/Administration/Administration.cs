﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Bot.Config;
using Bot.Config.Data;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Microsoft.Extensions.Logging;
using Shared;
using SixLabors.ImageSharp;

namespace Bot.Commands.Administration
{
    [Group("admin")]
    [Aliases("a")]
    [Description("Commands for administration and debugging")]
    public partial class Administration : BaseCommandModule
    {
        [Command("announce")]
        [Aliases("a")]
        [Description("Post a message containing the specified text to a channel")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Task Announce(CommandContext ctx, [RemainingText] [Description("The message to announce")]
            string text) => Announce(ctx, ctx.Channel, text);

        [Command("announce")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Task Announce(CommandContext ctx, [Description("Role to mention")] DiscordRole mention,
            [RemainingText] [Description("The message to announce")]
            string text) => Announce(ctx, mention, ctx.Channel, text);

        [Command("announce")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Task Announce(CommandContext ctx, [Description("Channel to post in")] DiscordChannel target,
            [RemainingText] [Description("The message to announce")]
            string text) => Announce(ctx, ctx.Guild.EveryoneRole, target, text);

        [Command("announce")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Announce(CommandContext ctx, [Description("Role to mention")] DiscordRole mention,
            [Description("Channel to post in")] DiscordChannel target,
            [RemainingText] [Description("The message to announce")]
            string text)
        {
            if (ctx.Channel.MethodEnabled(nameof(Announce)))
            {
                await ctx.RequireGuild();
                await ctx.TriggerTypingAsync();
                DiscordMember author = await ctx.Guild.GetMemberAsync(ctx.Message.Author.Id);
                Permissions authorPerms = author.PermissionsIn(target);
                if (mention == ctx.Guild.EveryoneRole &&
                    (authorPerms & Permissions.MentionEveryone) != Permissions.MentionEveryone ||
                    (authorPerms & Permissions.AccessChannels) != Permissions.AccessChannels ||
                    (authorPerms & Permissions.EmbedLinks) != Permissions.EmbedLinks ||
                    (authorPerms & Permissions.SendMessages) != Permissions.SendMessages)
                {
                    await ctx.RespondAsync("You don't have sufficient privileges");
                    return;
                }
                await target.SendMessageAsync(mention.Mention, new DiscordEmbedBuilder
                {
                    Author = new DiscordEmbedBuilder.EmbedAuthor
                    {
                        IconUrl = author.AvatarUrl,
                        Name = author.Username
                    },
                    Description = text
                }.Build());
                await ctx.RespondAsync("Done!");
            }
        }

        [Command("ping")]
        [Aliases("pong", "p")]
        [Description("Responds with \"Pong\" if the bot is active")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Ping(CommandContext ctx)
        {
            if (ctx.Channel.AstraEnabled())
                await ctx.RespondAsync($"Pong! ({Program.Client.Ping}ms)");
        }

        [Command("cooldown")]
        [RequirePermissions(Permissions.DeafenMembers)]
        [Description("Sets a custom cooldown for this channel")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Cooldown(CommandContext ctx, int cooldown)
        {
            if (ctx.Channel.MethodEnabled(nameof(Cooldown)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                if (cooldown <= 21600 && cooldown >= 0)
                {
                    await ctx.Channel.ModifyAsync(x => x.PerUserRateLimit = cooldown);
                    await ctx.RespondAsync($"Set cooldown to {cooldown} seconds.");
                    return;
                }
                await ctx.RespondAsync($"Invalid cooldown: {cooldown}");
            }
        }

        [Command("avatar")]
        [Aliases("icon", "av")]
        [Description("Gets the avatar of the specified user")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Avatar(CommandContext ctx, [Description("User to get icon from")] DiscordUser user)
        {
            await ctx.TriggerTypingAsync();
            WebClient wc = new();
            Image bmp = Image.Load(await wc.DownloadDataTaskAsync(user.AvatarUrl));
            MemoryStream ms = new();
            await bmp.SaveAsJpegAsync(ms);
            ms.Position = 0;
            await ctx.RespondAsync(
                new DiscordMessageBuilder().WithFile("avatar.jpg", ms).WithContent($"Avatar of {user.Username}"));
            wc.Dispose();
        }

        [Command("sudo")]
        [Description("Debug command - don't use!")]
        [Hidden]
        [RequireOwner]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task SudoAsync(CommandContext ctx, [Description("Member to sudo")] DiscordMember m,
            [Description("Command to sudo")] [RemainingText]
            string command)
        {
            await ctx.TriggerTypingAsync();
            await ctx.RequireGuild();
            if (ctx.Client.CurrentApplication.Owners.All(x => x.Id != ctx.User.Id))
            {
                await ctx.RespondAsync("You do not have permission to use this command!");
                return;
            }
            await ctx.CommandsNext.ExecuteCommandAsync(ctx.CommandsNext.CreateFakeContext(m, ctx.Channel, command,
                CommonConfig.Get().Overrides.Prefix, ctx.CommandsNext.FindCommand(command, out string _)));
        }

        [Command("stop")]
        [Description("Debug command - don't use!")]
        [Hidden]
        [RequireOwner]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Stop(CommandContext ctx) => Program.Exit = true;

        [Command("clean")]
        [Description("Debug command - don't use!")]
        [Hidden]
        [RequireOwner]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Clean(CommandContext ctx)
        {
            await ctx.TriggerTypingAsync();
            ctx.Client.Logger.LogInformation(Program.Astra,
                $"Cleaning... ({ctx.Member.DisplayName} in {(ctx.IsDm() ? "DMs" : ctx.Guild.Name)}/{ctx.Channel.Name})");
            // Filesystem
            foreach (string directory in Directory.GetDirectories(Path.Combine(PathResolver.BasePath,
                PathResolver.Guilds)))
            {
                ulong ul = ulong.Parse(Path.GetFileName(directory));
                if (!ctx.Client.Guilds.ContainsKey(ul))
                {
                    Directory.Delete(directory, true);
                    continue;
                }
                DiscordGuild g = await ctx.Client.GetGuildAsync(ul);
                foreach (string s in Directory.GetDirectories(Path.Combine(directory, PathResolver.Channels)))
                {
                    ul = ulong.Parse(Path.GetFileName(s));
                    if (!g.Channels.ContainsKey(ul)) Directory.Delete(s, true);
                }
            }
            // Objects
            CommonConfig cfg = CommonConfig.Get();
            CleanOverrides(cfg.Overrides, new ConfigOverrides[0]);
            cfg.Save();
            foreach (DiscordGuild guild in ctx.Client.Guilds.Values)
            {
                GuildConfig guildCfg = GuildConfig.Get(guild);
                CleanOverrides(guildCfg.Overrides, new[] {cfg.Overrides});
                // ReactionRoles and timed bans are cleaned in ProcessLoop
                foreach (ulong user in guildCfg.UserScores.Keys.ToArray())
                    if (!guild.Members.ContainsKey(user))
                        guildCfg.UserScores.Remove(user);
                guildCfg.Save();
                foreach (DiscordChannel channel in guild.Channels.Values)
                {
                    ChannelConfig channelCfg = ChannelConfig.Get(channel);
                    CleanOverrides(channelCfg.Overrides, new[] {cfg.Overrides, guildCfg.Overrides});
                    channelCfg.Save();
                }
            }
            await ctx.RespondAsync("Completed clean");
        }

        private void CleanOverrides(ConfigOverrides overrides, ConfigOverrides[] upper)
        {
            bool nsfwSafe = overrides.Nsfw == null;
            bool prefixSafe = overrides.Prefix == null;
            List<string> safeCommands = new();
            // Clean overrides contained in uppers
            foreach (ConfigOverrides ov in upper)
            {
                if (!nsfwSafe)
                    if (ov.Nsfw != null)
                    {
                        nsfwSafe = true;
                        if (ov.Nsfw == overrides.Nsfw)
                            overrides.Nsfw = null;
                    }
                if (!prefixSafe)
                    if (ov.Prefix != null)
                    {
                        prefixSafe = true;
                        if (ov.Prefix == overrides.Prefix)
                            overrides.Prefix = null;
                    }
                foreach (KeyValuePair<string, bool?> kvp in overrides.Command.ToArray())
                    if (safeCommands.Contains(kvp.Key))
                        continue;
                    else if (kvp.Value == null)
                        overrides.Command.Remove(kvp.Key);
                    else if (ov.Command.Any(s => s.Key == kvp.Key && s.Value != null))
                    {
                        safeCommands.Add(kvp.Key);
                        if (ov.Command[kvp.Key] == kvp.Value)
                            overrides.Command.Remove(kvp.Key);
                    }
            }
            // Other stuff
            string[] commands = CommandNameTranslator.GetConfigNames().ToArray();
            foreach (KeyValuePair<string, bool?> kvp in overrides.Command.ToArray())
                if (!commands.Contains(kvp.Key))
                    overrides.Command.Remove(kvp.Key);
        }

        [Command("purge")]
        [Description("Purge commands by user or regex")]
        [RequirePermissions(Permissions.ManageMessages)]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Purge(CommandContext ctx, [Description("User to delete posts from")]
            DiscordMember member, [Description("Amount of messages to search")]
            int span = 50, [Description("Reason for deletion")] string? reason = null)
        {
            if (ctx.Channel.MethodEnabled(nameof(Purge)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                IEnumerable<DiscordMessage> messages = await ctx.Channel.GetMessagesAsync(span);
                messages = messages.Where(s => ctx.Guild.Members.First(a => a.Key == s.Author.Id).Value == member);
                await ctx.Channel.DeleteMessagesAsync(messages, reason);
                await ctx.RespondAsync($"Deleted {messages.Count()} messages");
            }
        }

        [Command("purge")]
        [RequirePermissions(Permissions.ManageMessages)]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Purge(CommandContext ctx, [Description("Regex for messages")] string regex,
            [Description("Amount of messages to search")]
            int span, [Description("Reason for deletion")] string? reason = null)
        {
            if (ctx.Channel.MethodEnabled(nameof(Purge)))
            {
                await ctx.TriggerTypingAsync();
                IEnumerable<DiscordMessage> messages = await ctx.Channel.GetMessagesAsync(span);
                Regex rex = new(regex);
                messages = messages.Where(s => rex.IsMatch(s.Content));
                await ctx.Channel.DeleteMessagesAsync(messages, reason);
                await ctx.RespondAsync($"Deleted {messages.Count()} messages");
            }
        }
    }
}