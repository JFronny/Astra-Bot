using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Bot.Config;
using Bot.Config.Data;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Shared;

namespace Bot.Commands.Administration
{
    [Group("customcommands")]
    [Aliases("cc", "flags")]
    [Description("CustomCommands allows simple reactions/commands to be created")]
    public class CustomCommands : BaseCommandModule
    {
        public static readonly List<Param> Params = new()
        {
            new Param
            {
                Name = "n",
                Description = "Local of the author",
                StringSelector = s => s.Member.DisplayName
            },
            new Param
            {
                Name = "m",
                Description = "Mention of the author",
                StringSelector = s => s.Member.Mention
            }
        };

        private readonly Regex rg = new(@"^[a-zA-Z0-9]*$");

        [Command("add")]
        [RequirePermissions(Permissions.ManageGuild)]
        [Description("Create or replace a guild-wide command. You may use some params, see list-params for details")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task AddCC(CommandContext ctx, [Description("The command name")] string name,
            [Description("The command string")] [RemainingText]
            string command)
        {
            if (ctx.Channel.MethodEnabled(nameof(AddCC)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                if (!rg.IsMatch(name))
                {
                    await ctx.RespondAsync("The command name must be alphanumeric");
                    return;
                }
                if (CommandNameTranslator.GetGroupNames().Contains(name))
                {
                    await ctx.RespondAsync("Custom commands may not use a groups name");
                    return;
                }
                GuildConfig gc = GuildConfig.Get(ctx.Guild);
                if (!gc.CustomCommands.ContainsKey(name))
                    gc.CustomCommands.Add(name, command);
                gc.CustomCommands[name] = command;
                gc.Save();
                await ctx.RespondAsync("Successfully added command");
            }
        }

        [Command("remove")]
        [RequirePermissions(Permissions.ManageGuild)]
        [Description("Removes a custom command")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task RemoveCC(CommandContext ctx, [Description("The command name")] string name)
        {
            if (ctx.Channel.MethodEnabled(nameof(AddCC)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                GuildConfig gc = GuildConfig.Get(ctx.Guild);
                if (!gc.CustomCommands.Remove(name))
                {
                    await ctx.RespondAsync("Could not find command");
                    return;
                }
                gc.Save();
                await ctx.RespondAsync("Successfully removed command");
            }
        }

        [Command("list")]
        [Description("Lists all available custom commands")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task ListCC(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(ListCC)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                await ctx.RespondPaginatedIfTooLong(string.Join(", ", GuildConfig.Get(ctx.Guild).CustomCommands.Keys));
            }
        }

        [Command("list-params")]
        [Description("Lists all parameters available to custom commands")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task ListCCParams(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(ListCCParams)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                StringBuilder sb = new();
                foreach (Param param in Params)
                {
                    sb.Append("{{");
                    sb.Append(param.Name);
                    sb.Append("}}: ");
                    sb.Append(param.Description);
                    sb.Append(" (currently: ");
                    sb.Append(param.StringSelector(new CCContext(ctx.Member)));
                    sb.Append(")\n");
                }
                sb.Append("You can also access command parameters as {{0}}, {{1}}, etc or {{*}} for everything");
                await ctx.RespondAsync(sb.ToString());
            }
        }

        public class CCContext
        {
            public DiscordMember Member;

            public CCContext(DiscordMember member) => Member = member;
        }

        public class Param
        {
            public string Description;
            public string Name;
            public Func<CCContext, string> StringSelector;
        }
    }
}