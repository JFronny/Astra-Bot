using System;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Bot.Config;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using Newtonsoft.Json.Linq;
using Shared;

namespace Bot.Commands.Stats
{
    [Group("stat")]
    [Aliases("s")]
    [Description("Information that is not unique to this server")]
    public class PublicStats : BaseCommandModule
    {
        [Command("about")]
        [Description("Prints some info about the bot")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task About(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(About)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RespondAsync($@"Webpage: https://jfronny.gitlab.io/home/bot
Guide: https://jfronny.gitlab.io/Astra-Bot
Repo: https://gitlab.com/JFronny/Astra-Bot
Uptime: {(DateTime.Now - Program.Start).GetReadable()}");
            }
        }

        [Command("guildcount")]
        [Description("Gets the amount of connected Guilds")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task GuildCount(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(GuildCount)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RespondAsync($"Currently connected to {Program.Client.Guilds.Count} Guilds");
            }
        }

        [Command("changelog")]
        [Description("Gets the amount of connected Guilds")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Changelog(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(Changelog)))
            {
                await ctx.TriggerTypingAsync();
                using WebClient wc = new();
                JArray a = JArray.Parse(
                    await wc.DownloadStringTaskAsync(
                        "https://www.gitlab.com/api/v4/projects/19454790/repository/commits"));
                string t = "";
                for (int i = 0; i < a.Count; i++)
                {
                    t += a[i].Value<string>("message").Replace("\r", "").Replace("\n", " ") + "\n\n";
                    if (i > 10)
                        break;
                }
                await ctx.RespondAsyncFix(t);
            }
        }

        [Command("invite")]
        [Description("Gets the invite-link")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Invite(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(Invite)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RespondAsync(
                    $"Invite Link: https://discordapp.com/oauth2/authorize?client_id={ctx.Client.CurrentApplication.Id}&scope=bot&permissions=2147483647");
            }
        }

        [Command("gitlab")]
        [Aliases("contribute", "issue", "github")]
        [Description("Pastes the gitlab link")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Github(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(Github)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RespondAsync("Repo Link: https://gitlab.com/JFronny/Astra-Bot");
            }
        }

        [Command("website")]
        [Aliases("info")]
        [Description("Pastes the docs link")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Website(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(Website)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RespondAsync(@"Webpage: https://jfronny.gitlab.io/home/bot
Guide: https://jfronny.gitlab.io/Astra-Bot");
            }
        }

        [Command("uptime")]
        [Aliases("u")]
        [Description("Prints the bots uptime")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Uptime(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(Uptime)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RespondAsync($"Up for {(DateTime.Now - Program.Start).GetReadable()}");
            }
        }
    }
}