using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Bot.Config;
using Bot.Config.Data;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Shared;

namespace Bot.Commands.Stats
{
    [Group("info")]
    [Aliases("i")]
    [Description("Information that is unique to this server")]
    public class LocalStats : BaseCommandModule
    {
        [Command("user")]
        [Aliases("u")]
        [Description("Prints out information about the specified user")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task User(CommandContext ctx, [Description("User to print")] DiscordUser user)
        {
            if (ctx.Channel.MethodEnabled(nameof(User)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RespondPaginatedIfTooLong(string.Join("\n",
                    typeof(DiscordUser).GetProperties()
                        .Select(s =>
                        {
                            try
                            {
                                return $"{s.Name}: {s.GetValue(user)}";
                            }
                            catch
                            {
                                return $"Could not read {s.Name}";
                            }
                        })));
            }
        }

        [Command("member")]
        [Aliases("m")]
        [Description("Prints out information about the specified member")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Member(CommandContext ctx, [Description("Member to print")] DiscordMember member)
        {
            if (ctx.Channel.MethodEnabled(nameof(Member)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                await ctx.RespondPaginatedIfTooLong(string.Join("\n",
                    typeof(DiscordMember).GetProperties()
                        .Select(s =>
                        {
                            try
                            {
                                return $"{s.Name}: {s.GetValue(member)}";
                            }
                            catch
                            {
                                return $"Could not read {s.Name}";
                            }
                        })));
            }
        }

        [Command("role")]
        [Aliases("r")]
        [Description("Prints out information about the specified role")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Role(CommandContext ctx, [Description("Role to print")] DiscordRole role)
        {
            if (ctx.Channel.MethodEnabled(nameof(Role)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                await ctx.RespondPaginatedIfTooLong(string.Join("\n",
                    typeof(DiscordRole).GetProperties()
                        .Select(s =>
                        {
                            try
                            {
                                return $"{s.Name}: {s.GetValue(role)}";
                            }
                            catch
                            {
                                return $"Could not read {s.Name}";
                            }
                        })));
            }
        }

        [Command("channel")]
        [Aliases("c")]
        [Description("Prints out information about the specified channel")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Channel(CommandContext ctx, [Description("Channel to print, leave empty for current")]
            DiscordChannel? channel = null)
        {
            if (ctx.Channel.MethodEnabled(nameof(Channel)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                if (channel == null)
                    channel = ctx.Channel;
                StringBuilder sb = new();
                AppendOverrides(sb, ChannelConfig.Get(channel).Overrides);
                AppendProperties(sb, channel);
                await ctx.RespondPaginatedIfTooLong(sb.ToString());
            }
        }

        [Command("guild")]
        [Aliases("g")]
        [Description("Prints out information about the current guild")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Guild(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(Guild)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                StringBuilder sb = new();
                AppendOverrides(sb, GuildConfig.Get(ctx.Guild).Overrides);
                AppendProperties(sb, ctx.Guild);
                await ctx.RespondPaginatedIfTooLong(sb.ToString());
            }
        }

        private void AppendProperties<T>(StringBuilder sb, T source)
        {
            sb.Append("\n\nProperties:");
            foreach (PropertyInfo property in typeof(T).GetProperties())
            {
                sb.Append("\n");
                sb.Append(property.Name);
                sb.Append(": ");
                try
                {
                    sb.Append(property.GetValue(source));
                }
                catch
                {
                    sb.Append("[could not read]");
                }
            }
        }

        private void AppendOverrides(StringBuilder sb, ConfigOverrides ov)
        {
            sb.Append("Config Overrides:");
            if (ov.Prefix != null)
            {
                sb.Append("\nPrefix: ");
                sb.Append(ov.Prefix);
            }
            if (ov.Nsfw != null)
            {
                sb.Append("\nNsfw: ");
                sb.Append(ov.Nsfw.Value);
            }
            foreach (KeyValuePair<string, bool?> command in ov.Command)
                if (command.Value != null)
                {
                    sb.Append("\n");
                    sb.Append(command.Key);
                    sb.Append(": ");
                    sb.Append(command.Value.Value);
                }
        }
    }
}