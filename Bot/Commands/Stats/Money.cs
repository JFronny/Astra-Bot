﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Bot.Config;
using Bot.Config.Data;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Shared;

namespace Bot.Commands.Stats
{
    [Group("money")]
    [Aliases("$")]
    [Description("Commands to manage your money\nYou earn money by sending messages or by gambling (games)")]
    public class Money : BaseCommandModule
    {
        [Command("balance")]
        [Aliases("b")]
        [Description("Gets the money you have/someone has")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task GetMoney(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(GetMoney)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                await ctx.RespondAsync($"You have {ctx.Member.GetMoney()} coins");
            }
        }

        [Command("balance")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task GetMoney(CommandContext ctx, [Description("User whose balance to get")]
            DiscordMember user)
        {
            if (ctx.Channel.MethodEnabled(nameof(GetMoney)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                await ctx.RespondAsync(
                    $"{user.Username} has {user.GetMoney()} coins");
            }
        }

        [Command("balance")]
        [RequirePermissions(Permissions.Administrator)]
        [Description("Sets the money you have/someone has")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task SetMoney(CommandContext ctx, [Description("User whose balance to set")]
            DiscordMember user, [Description("New value")] long money)
        {
            if (ctx.Channel.MethodEnabled(nameof(SetMoney)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                decimal original = user.GetMoney();
                user.SetMoney(money);
                await ctx.RespondAsync(
                    $"{user.Username} now has {user.GetMoney()} coins instead of {original}");
            }
        }

        [Command("scoreboard")]
        [RequirePermissions(Permissions.Administrator)]
        [Description("Gets the members with the biggest wallet")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Scoreboard(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(Scoreboard)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                KeyValuePair<ulong, long>[] tmp = GuildConfig.Get(ctx.Guild).UserScores
                    .OrderByDescending(s => s.Value).ToArray();
                tmp = tmp.Length > 10 ? tmp.Where((s, i) => i < 10).ToArray() : tmp;
                await ctx.RespondAsync(
                    $"Richest members:\r\n{string.Join("\n", tmp.Select(s => $"{ctx.Guild.Members[s.Key].DisplayName}: {s.Value}"))}");
            }
        }

        [Command("give")]
        [Description("Give someone money")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task GiveMoney(CommandContext ctx, [Description("User to give money to")] DiscordMember user,
            [Description("Money to give")] long money)
        {
            if (ctx.Channel.MethodEnabled(nameof(GiveMoney)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                if (money > ctx.Member.GetMoney() || money < 0)
                {
                    await ctx.RespondAsync("You don't have that much");
                    return;
                }
                user.IncrementMoney(money);
                ctx.Member.IncrementMoney(-money);
                await ctx.RespondAsync($"Gave {user.Username} {money} coins.");
            }
        }
    }
}