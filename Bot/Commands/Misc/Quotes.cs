using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Bot.Config;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using Octokit;
using Shared;

namespace Bot.Commands.Misc
{
    [Group("quote")]
    [Aliases("q")]
    [Description("Random pieces of text from various sources")]
    public class Quotes : BaseCommandModule
    {
        private static string[] Beequotes;
        private static string[] Fortunequotes;
        private static string[] FortunequotesOff;

        public static async Task Initialize()
        {
            Console.Write("Downloading BeeMovie quotes...");
            try
            {
                using (WebClient client = new())
                {
                    client.Headers["User-Agent"] =
                        "Mozilla/5.0 (iPhone; CPU iPhone OS 12_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1";
                    Beequotes = client
                        .DownloadString(
                            "http://www.script-o-rama.com/movie_scripts/a1/bee-movie-script-transcript-seinfeld.html")
                        .Split(new[] {"<pre>"}, StringSplitOptions.None)[1]
                        .Split(new[] {"</pre>"}, StringSplitOptions.None)[0]
                        .Split(new[] {"\n\n  \n"}, StringSplitOptions.None);
                }
                Beequotes[0] = Beequotes[0].Replace("  \n  \n", "");
                Console.WriteLine(" Finished.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Console.Write("Downloading fortunes");
            try
            {
                Fortunequotes = await GetFortuneQuotes("fortune-mod/datfiles");
                FortunequotesOff = await GetFortuneQuotes("fortune-mod/datfiles/off/unrotated");
            }
            catch (Exception e)
            {
                Console.WriteLine(
                    $"Cought {e.GetType().Name} while downloading. Fortunes might not be available. Restart in ~ an hour to fix");
            }
            Console.WriteLine(" Finished.");
        }

        public static async Task<string[]> GetFortuneQuotes(string path)
        {
            Console.Write(".");
            IEnumerable<RepositoryContent> files =
                await Program.Github.Repository.Content.GetAllContents("shlomif", "fortune-mod", path);
            Console.Write(".");
            IEnumerable<string?> disallowednames = new[] {"CMakeLists.txt", null};
            Console.Write(".");
            IEnumerable<RepositoryContent> filteredFiles =
                files.Where(s => s.Type == ContentType.File && !disallowednames.Contains(s.Name));
            Console.Write(".");
            IEnumerable<string> cookies =
                filteredFiles.Where(s => !disallowednames.Contains(s.Name)).Select(s => s.DownloadUrl);
            Console.Write(".");
            IEnumerable<string> contents;
            using WebClient client = new();
            contents = cookies.Select(s =>
            {
                Console.Write(".");
                return client.DownloadString(s);
            });
            Console.Write(".");
            return contents.SelectMany(s => s.Split(new[] {"\n%\n"}, StringSplitOptions.None)).ToArray();
        }

        [Command("fortune")]
        [Description("Spits out a quote")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Fortune(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(Fortune)))
            {
                await ctx.TriggerTypingAsync();
                string[] quotes =
#if !NO_NSFW
                    ctx.Channel.GetEvaluatedNsfw()
                        ? FortunequotesOff
                        :
#endif
                        Fortunequotes;
                await ctx.RespondAsyncFix(quotes[Program.Rnd.Next(quotes.Length)], true);
            }
        }

        [Command("beemovie")]
        [Description("Sends a quote from the bee movie script as TTS")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Beemovie(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(Beemovie)))
            {
                await ctx.TriggerTypingAsync();
                int q = Program.Rnd.Next(Beequotes.Length - 2);
                await ctx.RespondAsyncFix(
                    (Beequotes[q] + "\n\n" + Beequotes[q + 1] + "\n\n" + Beequotes[q + 2]).Replace("\n", "\r\n"),
                    true);
            }
        }
    }
}