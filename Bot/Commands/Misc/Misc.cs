﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Bot.Config;
using Bot.Converters;
using CC_Functions.Core;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.EventHandling;
using DSharpPlus.Interactivity.Extensions;
using Shared;
using Shared.QR;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Drawing;
using SixLabors.ImageSharp.Drawing.Processing;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace Bot.Commands.Misc
{
    [Group("misc")]
    [Aliases("r")]
    [Description("Random commands that didn't fit into other categories")]
    public class Misc : BaseCommandModule
    {
        private static readonly string[] AnswerList =
        {
            "IT IS\nCERTAIN",
            "IT IS\nDECIDEDLY\nSO",
            "YES\nDEFINITELY",
            "YOU\nMAY RELY\nON IT",
            "AS I\nSEE IT,\nYES",
            "MOST\nLIKELY",
            "YES",
            "REPLY HAZY,\nTRY AGAIN",
            "ASK\nAGAIN\nLATER",
            "DON'T\nCOUNT\nON IT",
            "VERY\nDOUBTFUL",
            "WITHOUT A\nDOUBT",
            "OUTLOOK\nGOOD",
            "SIGNS\nPOINT TO\nYES",
            "BETTER\nNOT TELL\nYOU NOW",
            "CANNOT\nPREDICT\nNOW",
            "CONCENTRATE\nAND ASK\nAGAIN",
            "MY REPLY\nIS NO",
            "MY SOURCES\nSAY NO",
            "OUTLOOK\nNOT SO\nGOOD"
        };

        [Command("poll")]
        [Description(
            "Run a poll with reactions")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Poll(CommandContext ctx, [Description("What to ask")] string text,
            [Description("How long should the poll last.")]
            TimeSpan duration, [Description("What options should people have.")]
            params DiscordEmoji[] options)
        {
            if (ctx.Channel.MethodEnabled(nameof(Poll)))
            {
                await ctx.TriggerTypingAsync();
                if (duration > new TimeSpan(5, 0, 0))
                    throw new ArgumentOutOfRangeException("Please choose a smaller time");
                DiscordMessage msg = await ctx.RespondAsync(new DiscordEmbedBuilder
                {
                    Title = "Poll time!",
                    Description = text
                }.Build());
                ReadOnlyCollection<PollEmoji> pollResult = await Program.Client.GetInteractivity()
                    .DoPollAsync(msg, options, timeout: duration);
                IEnumerable<string> results = pollResult.Where(xkvp => options.Contains(xkvp.Emoji))
                    .Select(xkvp => $"{xkvp.Emoji}: {xkvp.Voted.Count}");
                await ctx.RespondAsync(string.Join("\n", results));
            }
        }

        [Command("quicktype")]
        [Description("Waits for a response containing a generated code")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task QuickType(CommandContext ctx,
            [Description("Bytes to generate. One byte equals two characters")]
            int bytes, [Description("Time before exiting")] TimeSpan time)
        {
            if (ctx.Channel.MethodEnabled(nameof(QuickType)))
            {
                await ctx.TriggerTypingAsync();
                if (time > new TimeSpan(0, 1, 0))
                    throw new ArgumentOutOfRangeException("Please choose a smaller time");
                InteractivityExtension interactivity = ctx.Client.GetInteractivity();
                byte[] codeBytes = new byte[bytes];
                Program.Rnd.NextBytes(codeBytes);
                string code = BitConverter.ToString(codeBytes).ToLower().Replace("-", "");
                await ctx.RespondAsync($"The first one to type the following code gets a reward: {code.Emotify()}");
                InteractivityResult<DiscordMessage> msg =
                    await interactivity.WaitForMessageAsync(xm => xm.Content.Contains(code), time);
                if (msg.TimedOut)
                    await ctx.RespondAsync("Nobody? Really?");
                else
                    await ctx.RespondAsync($"And the winner is: {msg.Result.Author.Mention}");
            }
        }

        [Command("emotify")]
        [Description("Converts your text to emoticons")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Emotify(CommandContext ctx, [Description("What should be converted")] [RemainingText]
            string text)
        {
            if (ctx.Channel.MethodEnabled(nameof(Emotify)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RespondAsync(text.Emotify());
            }
        }

        [Command("leetify")]
        [Aliases("1337ify")]
        [Description("Leetifies your text")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Leetify(CommandContext ctx, [Description("What should be leetified")] [RemainingText]
            string text)
        {
            if (ctx.Channel.MethodEnabled(nameof(Leetify)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RespondAsyncFix(text.Leetify());
            }
        }

        [Command("random")]
        [Aliases("rnd", "rng", "dice", "r")]
        [Description("Generates a random number")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Random(CommandContext ctx, [Description("The inclusive minimum for the number")]
            int minimum, [Description("The inclusive maximum for the number")]
            int maximum)
        {
            if (ctx.Channel.MethodEnabled(nameof(Random)))
            {
                await ctx.TriggerTypingAsync();
                if (maximum < minimum)
                {
                    int tmp = maximum;
                    maximum = minimum;
                    minimum = tmp;
                }
                await ctx.RespondPaginatedIfTooLong(Program.Rnd.Next(minimum, maximum + 1).ToString());
            }
        }

        [Command("coinflip")]
        [Aliases("flip")]
        [Description("Flip a coin")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Coinflip(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(Coinflip)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RespondPaginatedIfTooLong(Program.Rnd.Next(0, 2) == 0 ? "Head" : "Tail");
            }
        }

        [Command("magic8")]
        [Description("The answer to your questions")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Magic8(CommandContext ctx, [Description("Question to answer")] [RemainingText]
            string question)
        {
            if (ctx.Channel.MethodEnabled(nameof(Magic8)))
            {
                await ctx.TriggerTypingAsync();
                Rectangle size = new(0, 0, 400, 400);
                using Image image = new Image<Rgba32>(size.Width, size.Height);
                image.Mutate(s =>
                {
                    s.Clear(SixLabors.ImageSharp.Color.White);
                    s.Fill(SixLabors.ImageSharp.Color.Black,
                        new EllipsePolygon(size.Location + size.Size / 2, size.Size));
                    size.Width /= 2;
                    size.Height /= 2;
                    size.X = size.Width / 2;
                    size.Y = size.Height / 2;
                    s.Fill(SixLabors.ImageSharp.Color.FromRgb(80, 80, 80),
                        new EllipsePolygon(size.Location + size.Size / 2, size.Size));
                    PointF center = new(size.X + size.Width / 2, size.Y + size.Height / 2);
                    float radius = size.Width / 2f;
                    s.FillPolygon(SixLabors.ImageSharp.Color.Blue,
                        new PointF(center.X - 0.866f * radius, center.Y - 0.5f * radius),
                        new PointF(center.X + 0.866f * radius, center.Y - 0.5f * radius),
                        new PointF(center.X, center.Y + radius));
                    Font font = SystemFonts.Families.First().CreateFont(10);
                    font = new Font(font,
                        1500f / TextMeasurer.Measure("QWERTBTESTSTR", new RendererOptions(font)).Width);
                    string answer = AnswerList[Program.Rnd.Next(AnswerList.Length)];
                    s.DrawText(new TextGraphicsOptions(new GraphicsOptions(), new TextOptions
                    {
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center
                    }), answer, font, SixLabors.ImageSharp.Color.White, center);
                });
                await using MemoryStream str = new();
                await image.SaveAsJpegAsync(str);
                str.Position = 0;
                await ctx.RespondAsync(new DiscordMessageBuilder().WithFile("Magic8.jpg", str));
            }
        }

        [Command("unshorten")]
        [Description("Unshorten a fishy URL")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Unshorten(CommandContext ctx, [Description("URL to unshorten")] Uri url)
        {
            if (ctx.Channel.MethodEnabled(nameof(Unshorten)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RespondAsyncFix($"Response is: {url.Unshorten().AbsoluteUri}");
            }
        }

        [Command("toxicity")]
        [Aliases("tox")]
        [Description("Calculate the specified users toxicity")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Toxicity(CommandContext ctx, [Description("Member to calculate")] DiscordMember member)
        {
            if (ctx.Channel.MethodEnabled(nameof(Toxicity)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RequireGuild();
                IReadOnlyList<DiscordMessage> msg = await ctx.Channel.GetMessagesAsync();
                IEnumerable<string?> messages = msg.Where(s => s.Author.Id == member.Id).Select(x => x.Content);
                IEnumerable<DiscordEmbed> embeds = msg.Where(s => s.Embeds != null).SelectMany(s => s.Embeds);
                messages = messages.Concat(embeds
                    .SelectMany(s => new[] {s.Title, s.Description}
                        .Concat((s.Fields ?? new DiscordEmbedField[0]).SelectMany(a => new[] {a.Name, a.Value})))
                );
                messages = messages.Where(s => !string.IsNullOrWhiteSpace(s));
                string str = string.Join("\n", messages);
                await ctx.RespondAsyncFix(
                    $"Toxicity: {(await Program.Perspective.RequestAnalysis(str)).AttributeScores.First().Value.SummaryScore.Value}");
            }
        }

        [Command("minecraft")]
        [Aliases("mc")]
        [Description("Gets the status of a minecraft server")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Minecraft(CommandContext ctx, [Description("IP of the server")] string ip)
        {
            if (ctx.Channel.MethodEnabled(nameof(Minecraft)))
            {
                await ctx.TriggerTypingAsync();
                string[] parsedIp = ip.Split(':');
                ushort port = parsedIp.Length == 1 ? (ushort) 25565 : ushort.Parse(parsedIp[1]);
                await ctx.RespondAsyncFix(StatReader.ReadStat(parsedIp[0], port));
            }
        }

        [Command("quote")]
        [Aliases("q")]
        [Description("Quote a message")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Quote(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(Quote)))
            {
                await ctx.TriggerTypingAsync();
                DiscordMessage msg = (await ctx.Channel.GetMessagesAsync()).Skip(1).First(s => !s.Author.IsBot);
                await msg.Quote(ctx, s => s.GetEvaluatedNsfw());
            }
        }

        [Command("quote")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Quote(CommandContext ctx, [Description("The messages ID")] ulong id)
        {
            if (ctx.Channel.MethodEnabled(nameof(Quote)))
            {
                await ctx.TriggerTypingAsync();
                DiscordMessage msg = await ctx.Channel.GetMessageAsync(id);
                await msg.Quote(ctx, s => s.GetEvaluatedNsfw());
            }
        }

        [Command("quote")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Quote(CommandContext ctx, [Description("A link to the message")] DiscordMessage message)
        {
            if (ctx.Channel.MethodEnabled(nameof(Quote)))
            {
                await ctx.TriggerTypingAsync();
                await message.Quote(ctx, s => s.GetEvaluatedNsfw());
            }
        }

        [Command("color")]
        [Aliases("col", "colour")]
        [Description("Show info about a color. Chooses a random color when none is specified")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Color(CommandContext ctx, [Description("The color")] DiscordColor? color = null)
        {
            if (ctx.Channel.MethodEnabled(nameof(Color)))
            {
                await ctx.TriggerTypingAsync();
                color ??= DiscordNamedColorConverter.ColorNames.Values.OrderBy(s => Program.Rnd.NextDouble()).First();
                Color c = SixLabors.ImageSharp.Color.FromRgb(color.Value.R, color.Value.G, color.Value.B);
                using Image image = new Image<Rgba32>(100, 100);
                image.Mutate(s => s.Clear(c));
                await using MemoryStream str = new();
                await image.SaveAsJpegAsync(str);
                str.Position = 0;
                //Data
                DiscordEmbedBuilder bld = new() {Color = color.Value};
                if (DiscordNamedColorConverter.ColorNames.Values.Any(s => s.Value == color.Value.Value))
                    bld.Title = DiscordNamedColorConverter.ColorNames.First(s => s.Value.Value == color.Value.Value)
                        .Key;
                bld.AddField("RGB", $"{color.Value.R}, {color.Value.G}, {color.Value.B}", true);
                bld.AddField("Hex", $"{c.ToHex()}", true);
                bld.AddField("RGB Int", color.Value.Value.ToString(), true);
                await ctx.RespondAsync(new DiscordMessageBuilder().WithFile("Color.jpg", str).WithEmbed(bld.Build()));
            }
        }

        [Command("base64")]
        [Aliases("b64", "base")]
        [Description("Encode/Decode base64 strings (you can also attach a text file)")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Base64(CommandContext ctx, [Description("Whether to encode (default) or decode")]
            bool decode, [Description("The text to process")] [RemainingText]
            string text)
        {
            if (ctx.Channel.MethodEnabled(nameof(Base64)))
            {
                await ctx.TriggerTypingAsync();
                if (string.IsNullOrWhiteSpace(text) && ctx.Message.Attachments.Count > 0)
                {
                    using WebClient wc = new();
                    text = wc.DownloadString(ctx.Message.Attachments[0].Url);
                }
                if (decode)
                {
                    byte[] data = Convert.FromBase64String(text);
                    try
                    {
                        await using MemoryStream ms = new(data);
                        using StreamReader rd = new(ms);
                        await ctx.RespondAsync(await rd.ReadToEndAsync());
                    }
                    catch
                    {
                        await using MemoryStream ms = new(data);
                        await ctx.RespondAsync(new DiscordMessageBuilder().WithFile("blob.bin", ms)
                            .WithContent("Could not decode to string, using raw data"));
                    }
                }
                else
                    await ctx.RespondAsync(Convert.ToBase64String(Encoding.UTF8.GetBytes(text)));
            }
        }

        [Command("echo")]
        [Aliases("repeat")]
        [Description("Repeat a string back to you")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Echo(CommandContext ctx, [Description("The text to echo")] [RemainingText]
            string text)
        {
            if (ctx.Channel.MethodEnabled(nameof(Echo)))
                await ctx.RespondAsync(text);
        }

        [Command("reverse")]
        [Aliases("rev")]
        [Description("Reverse a string")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Task Reverse(CommandContext ctx, [Description("The text to echo")] [RemainingText]
            string text) => Reverse(ctx, false, text);

        [Command("reverse")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Task Reverse(CommandContext ctx, bool reverseWords, [Description("The text to echo")] [RemainingText]
            string text)
        {
            if (ctx.Channel.MethodEnabled(nameof(Reverse)))
                return ctx.RespondAsync(reverseWords
                    ? string.Join(' ', text.Split(' ').Reverse())
                    : string.Concat(text.Reverse()));
            return Task.CompletedTask;
        }

        [Command("generate-qr")]
        [Aliases("gen-qr", "mkqr")]
        [Description("Generate a QR Code from supplied data (string or attachment)")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task GenerateQr(CommandContext ctx, [Description("The text to echo")] [RemainingText]
            string text)
        {
            if (ctx.Channel.MethodEnabled(nameof(GenerateQr)))
            {
                await ctx.TriggerTypingAsync();
                Image bmp;
                if (string.IsNullOrWhiteSpace(text) && ctx.Message.Attachments.Count > 0)
                {
                    using WebClient wc = new();
                    QrCodeData data = QrGenerator.GenerateQrCode(wc.DownloadData(ctx.Message.Attachments[0].Url),
                        QrGenerator.EccLevel.Q);
                    QrCode code = new(data);
                    bmp = code.GetGraphic(20);
                }
                else
                {
                    QrCodeData data = QrGenerator.GenerateQrCode(text, QrGenerator.EccLevel.Q);
                    QrCode code = new(data);
                    bmp = code.GetGraphic(20);
                }
                await using MemoryStream str = new();
                await bmp.SaveAsJpegAsync(str);
                str.Position = 0;
                await ctx.RespondAsync(new DiscordMessageBuilder().WithFile("Code.jpg", str));
            }
        }

        [Command("hash")]
        [Aliases("gen-hash")]
        [Description(
            "Generate a Hash Code from supplied data (string or attachment), no arguments for a list of possible algorithms")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Hash(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(Hash)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RespondAsync(string.Join(", ",
                    Enum.GetValues<HashType>().Select(s => s.ToString())));
            }
        }

        [Command("hash")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Hash(CommandContext ctx,
            [Description("The algorithm to use")] HashType algorithm,
            [Description("The text to echo")] [RemainingText]
            string text)
        {
            if (ctx.Channel.MethodEnabled(nameof(Hash)))
            {
                await ctx.TriggerTypingAsync();
                byte[] data;
                using WebClient wc = new();
                data = string.IsNullOrWhiteSpace(text) && ctx.Message.Attachments.Count > 0
                    ? wc.DownloadData(ctx.Message.Attachments[0].Url)
                    : Encoding.UTF8.GetBytes(text);
                HashAlgorithm impl = algorithm switch
                {
                    HashType.Md5 => new MD5CryptoServiceProvider(),
                    HashType.Sha1 => new SHA1Managed(),
                    HashType.Sha256 => new SHA256Managed(),
                    _ => throw new ArgumentOutOfRangeException()
                };
                byte[] hash = impl.ComputeHash(data);
                await ctx.RespondAsync(BitConverter.ToString(hash).Replace("-", string.Empty).ToUpper());
            }
        }
    }
}