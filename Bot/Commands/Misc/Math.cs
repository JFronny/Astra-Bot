using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Bot.Config;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using org.mariuszgromada.math.mxparser;
using Shared;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Drawing.Processing;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace Bot.Commands.Misc
{
    [Group("math")]
    [Aliases("m", "+")]
    [Description("Commands for calculating. Also includes money conversion")]
    public class Math : BaseCommandModule
    {
        [Command("calc")]
        [Description(
            "Calculates a result using mathparser.org. Example: \"sin(15^2)\", \"15 * (-12)\", \"solve( 2 * x - 4, x, 0, 10 )\", \"log(4, 2)\" - Please note: sin() etc use radians! 2*pi radians equals 360°")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Calc(CommandContext ctx, [Description("Equation")] [RemainingText]
            string equation)
        {
            if (ctx.Channel.MethodEnabled(nameof(Calc)))
            {
                await ctx.TriggerTypingAsync();
                Expression ex = new(equation);
#if VERBOSE
                ex.setVerboseMode();
#endif
                double result = ex.calculate();
                await ctx.RespondAsyncFix(double.IsNaN(result)
                    ? ex.getErrorMessage()
                    : $"{ex.getExpressionString()} = {result}");
            }
        }

        [Command("solve")]
        [Description(
            "Solve a mathematical function. See calc for extra help. Example: \"solve x -100 100 3 * x * 2 = 15 * x\"")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Solve(CommandContext ctx, [Description("Variable to calculate")] string target,
            [Description("Minimum value for result")]
            string min, [Description("Maximum value for result")]
            string max, [Description("Equation")] [RemainingText]
            string equation)
        {
            if (ctx.Channel.MethodEnabled(nameof(Solve)))
            {
                await ctx.TriggerTypingAsync();
                string[] parts = equation.Split('=');
                string newEq = $"({parts[0].Trim()}) - ({parts[1].Trim()})";
                newEq = $"solve({newEq}, {target}, {min}, {max})";
                Expression ex = new(newEq);
                await ctx.RespondAsyncFix($"{ex.getExpressionString()} = {ex.calculate()}");
            }
        }

        [Command("graph")]
        [Description(
            "Generates a x-based graph (variable x will be set), see \"calc\" for syntax. Example: graph x + 15")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Graph(CommandContext ctx, [Description("Equation")] [RemainingText]
            string equation)
        {
            if (ctx.Channel.MethodEnabled(nameof(Graph)))
            {
                await ctx.TriggerTypingAsync();
                Pen grid = Pens.Solid(Color.LightGray, 1);
                Pen gridZero = Pens.Solid(Color.Gray, 1);
                Pen line = Pens.Solid(Color.Red, 1);
                using Image image = new Image<Rgb24>(200, 200);
                image.Mutate(s =>
                {
                    s.Clear(Color.White);
                    for (int i = -100; i < 100; i += 10)
                    {
                        s.DrawLines(i == 0 ? gridZero : grid, new PointF(i + 100, 200), new PointF(i + 100, 0));
                        s.DrawLines(i == 0 ? gridZero : grid, new PointF(200, 100 - i), new PointF(0, 100 - i));
                    }
                    List<PointF> points = new();
                    for (int x = -100; x < 100; x++)
                    {
                        double result = new Expression(equation, new Argument("x", x / 10d)).calculate();
                        result *= 10;
                        if (!double.IsNaN(result) && result <= 200 && result >= -200)
                            points.Add(new PointF(x + 100, 100 - Convert.ToSingle(result)));
                    }
                    s.DrawLines(line, points.ToArray());
                });
                await using MemoryStream memoryStream = new();
                await image.SaveAsJpegAsync(memoryStream);
                memoryStream.Position = 0;
                await ctx.RespondAsync(new DiscordMessageBuilder().WithFile("EquationResult.jpg", memoryStream));
            }
        }

        [Command("currency")]
        [Description("Transforms currencies")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Currency(CommandContext ctx, [Description("Input currency in ISO")] Currency inCurrency,
            [Description("Output currency in ISO")]
            Currency outCurrency, [Description("Amount to convert")] double amount)
        {
            if (ctx.Channel.MethodEnabled(nameof(Currency)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RespondAsync(
                    $"{amount} {inCurrency.CurrencyName} equals {CurrencyConverter.Convert(amount, inCurrency, outCurrency, TokenData.Get().CurrConvToken)} {outCurrency.CurrencyName}");
            }
        }

        [Command("currency")]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public async Task Currency(CommandContext ctx)
        {
            if (ctx.Channel.MethodEnabled(nameof(Currency)))
            {
                await ctx.TriggerTypingAsync();
                await ctx.RespondPaginatedIfTooLong(string.Join(", ",
                    CurrencyConverter.GetCurrencies(TokenData.Get().CurrConvToken).Values
                        .Select(s => $"{s.CurrencyName}/{s.CurrencySymbol} ({s.Id})")
                        .OrderBy(s => s)));
            }
        }
    }
}