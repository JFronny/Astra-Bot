using System;
using System.Reflection;
using DSharpPlus.CommandsNext.Attributes;

namespace MDExtractor
{
    public static class GroupPage
    {
        public static string GenerateMd(Type group, out string name, string suffix, bool trimLinks)
        {
            name = group.GetAtt<GroupAttribute>().Name;
            string description = group.GetAtt<DescriptionAttribute>().Description;
            string md = $@"# {name} [⌂](../{(trimLinks ? "" : $"index{suffix}")})
{description}
";
            if (group.HasAtt<AliasesAttribute>())
            {
                md += @"
Aliases:";
                foreach (string alias in group.GetAtt<AliasesAttribute>().Aliases)
                    md += $@"
- `{alias}`";
            }
            md += @"

Commands:";
            foreach (MethodInfo command in group.GetCommands())
            {
                md += $@"
- [`{command.GetAtt<CommandAttribute>().Name}`]({command.GetAtt<CommandAttribute>().Name}{(trimLinks ? "" : suffix)}): {command.GetAtt<DescriptionAttribute>().Description}";
                if (command.HasAtt<RequirePermissionsAttribute>())
                    md += $@"\
Requires `{command.GetAtt<RequirePermissionsAttribute>().Permissions}`";
                if (command.HasAtt<RequireOwnerAttribute>())
                    md += @"\
Server owner only";
            }
            return md;
        }
    }
}