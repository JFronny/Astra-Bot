using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DSharpPlus.CommandsNext.Attributes;

namespace MDExtractor
{
    public static class DataExtractor
    {
        public static IEnumerable<Type> GetGroups(this Assembly bot) => bot.GetTypes()
            .Where(s => s.HasAtt<GroupAttribute>() && !s.HasAtt<HiddenAttribute>())
            .OrderBy(s => s.GetAtt<GroupAttribute>().Name);

        public static IEnumerable<MethodInfo> GetCommands(this Type s) => s.GetMethods()
            .Where(P.HasAtt<CommandAttribute>)
            .Where(s => !s.HasAtt<HiddenAttribute>())
            .GroupBy(s => s.GetAtt<CommandAttribute>().Name).Select(s => s.First())
            .OrderBy(s => s.GetAtt<CommandAttribute>().Name);

        public static IEnumerable<MethodInfo> GetOverloads(this MethodInfo info) =>
            info.DeclaringType.GetMethods().Where(P.HasAtt<CommandAttribute>)
                .Where(s => !s.HasAtt<HiddenAttribute>())
                .GroupBy(s => s.GetAtt<CommandAttribute>().Name)
                .First(s => s.First().GetAtt<CommandAttribute>().Name == info.GetAtt<CommandAttribute>().Name);
    }
}