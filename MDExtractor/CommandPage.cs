using System;
using System.Linq;
using System.Reflection;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

namespace MDExtractor
{
    public static class CommandPage
    {
        public static string GenerateMd(MethodInfo command, out string name, string suffix, string groupName,
            bool trimLinks)
        {
            name = command.GetAtt<CommandAttribute>().Name;
            string md = $@"# `{groupName} {name}` [⌂]({(trimLinks ? "." : $"index.{suffix}")})
{command.GetAtt<DescriptionAttribute>().Description}
";
            if (command.HasAtt<AliasesAttribute>())
            {
                md += @"
Aliases:";
                foreach (string alias in command.GetAtt<AliasesAttribute>().Aliases)
                    md += $@"
- `{alias}`";
            }
            md += @"

Overloads:";
            foreach (MethodInfo overload in command.GetOverloads())
            {
                md += $@"
- {string.Join(" ", overload.GetParameters().Where(s => s.ParameterType != typeof(CommandContext)).Select(s => $"`{s.Name}`"))}";
                foreach (ParameterInfo parameter in overload.GetParameters())
                    if (parameter.ParameterType != typeof(CommandContext))
                    {
                        md += $@"
  - `{parameter.Name}` ({parameter.ParameterType.GetName()})";
                        if (parameter.HasAtt<DescriptionAttribute>())
                            md += $@"\
{parameter.GetAtt<DescriptionAttribute>().Description}";
                        if (parameter.HasDefaultValue)
                            md += $@"\
Default: {parameter.DefaultValue}";
                    }
                if (command.HasAtt<RequirePermissionsAttribute>())
                    md += $@"\
Requires `{command.GetAtt<RequirePermissionsAttribute>().Permissions}`";
                if (command.HasAtt<RequireOwnerAttribute>())
                    md += @"\
Server owner only";
            }
            return md;
        }

        private static string GetName(this Type p)
        {
            string tmp = p.Name;
            if (tmp.StartsWith("Discord"))
                tmp = tmp.Substring(7);
            if (p.BaseType == typeof(Array) && p.GetElementType() != typeof(string))
                return $"{GetName(p.GetElementType())} collection";
            return tmp switch
            {
                "String" => "Text",
                "Boolean" => "True/False",
                "String[]" => "Words",
                "ABooru" => "Booru",
                "DoujinEnum" => "NonBooru source",
                "Double" => "Number",
                "Decimal" => "Number",
                "Int32" => "Round Number",
                "UInt64" => "Round Number",
                "RpsOption" => "R/P/S",
                "TimeSpan" => "Time Span",
                "HashType" => "Hash Algorithm",
                "Nullable`1" => $"{Nullable.GetUnderlyingType(p).GetName()} or empty",
                _ => tmp
            };
        }
    }
}