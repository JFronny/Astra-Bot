﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Bot;
using Markdig;

namespace MDExtractor
{
    internal static class P
    {
        private static readonly MarkdownPipeline Pipeline =
            new MarkdownPipelineBuilder().UseAdvancedExtensions().Build();

        private static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Output MD");
                Console.Write("> ");
                args = new[] {Console.ReadLine()};
            }
            string path = args[0];
            if (!Directory.Exists(path) && !Directory.Exists(Path.GetDirectoryName(path)))
            {
                Console.WriteLine("Please provide a valid path");
                return;
            }
            bool complex = Directory.Exists(path) || string.IsNullOrWhiteSpace(Path.GetExtension(path));
            if (Directory.Exists(path))
            {
                if (!args.Has("ov"))
                {
                    Console.WriteLine("Overwrite existing directory? (y/n)");
                    Console.Write("> ");
                    ConsoleKey k;
                    do
                        k = Console.ReadKey().Key;
                    while (k != ConsoleKey.Y && k != ConsoleKey.N);
                    if (k == ConsoleKey.N)
                    {
                        Console.WriteLine("Cancelled.");
                        return;
                    }
                }
                Directory.Delete(path, true);
            }
            bool trimLinks = args.Has("trim-links");
            bool html = args.Has("html");
            Assembly bot = typeof(Program).Assembly;
            WriteF(complex ? Path.Combine(path, "index") : path,
                FeatureList.GenerateMd(bot, complex, Suffix(html), trimLinks), html);
            if (complex)
                foreach (Type g in bot.GetGroups())
                {
                    string md = GroupPage.GenerateMd(g, out string name, Suffix(html), trimLinks);
                    WriteF(Path.Combine(path, name, "index"), md, html);
                    foreach (MethodInfo command in g.GetCommands())
                    {
                        md = CommandPage.GenerateMd(command, out string pname, Suffix(html), name, trimLinks);
                        WriteF(Path.Combine(path, name, pname), md, html);
                    }
                }
        }

        public static T GetAtt<T>(this MemberInfo info) where T : Attribute =>
            (T) info.GetCustomAttributes(true).First(s => s is T);

        public static bool HasAtt<T>(this MemberInfo info) where T : Attribute =>
            info.GetCustomAttributes(true).Any(s => s is T);

        public static T GetAtt<T>(this ParameterInfo info) where T : Attribute =>
            (T) info.GetCustomAttributes(true).First(s => s is T);

        public static bool HasAtt<T>(this ParameterInfo info) where T : Attribute =>
            info.GetCustomAttributes(true).Any(s => s is T);

        private static bool Has(this string[] args, string key) => args.Any(s =>
            string.Equals(s.TrimStart('-', '/').ToLower(), key.ToLower(), StringComparison.InvariantCultureIgnoreCase));

        private static void WriteF(string path, string md, bool html)
        {
            if (!Directory.Exists(Path.GetDirectoryName(path)))
                Directory.CreateDirectory(Path.GetDirectoryName(path));
            File.WriteAllText(path + Suffix(html), html ? Markdown.ToHtml(md, Pipeline) : md);
        }

        private static string Suffix(bool html) => html ? ".html" : ".md";
    }
}