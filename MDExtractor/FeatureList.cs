using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using DSharpPlus.CommandsNext.Attributes;

namespace MDExtractor
{
    public static class FeatureList
    {
        public static string GenerateMd(Assembly bot, bool link, string suffix, bool trimLinks)
        {
            string md = @"# Astra-Bot [Autodoc](https://gitlab.com/JFronny/Astra-Bot/-/tree/master/MDExtractor)
A powerful multi-purpose discord bot!
[![Invite](https://img.shields.io/badge/-Invite-success)](https://discordapp.com/oauth2/authorize?client_id=644947101041557554&scope=bot&permissions=4294443006)
[![Discord](https://img.shields.io/discord/466965965658128384?label=Discord)](https://discord.gg/UjhHBqt)
[![GitLab](https://img.shields.io/badge/-GitLab-informational)](https://gitlab.com/JFronny/Astra-Bot)

Note:\
I am aware that the link includes a lot of permissions. You may disable everything you don't need.

Features:";
            IEnumerable<Type> groups = bot.GetGroups();
            md += $@"
- {groups.SelectMany(s => s.GetCommands()).Count()} powerful commands (state: {DateTime.Now.ToString(CultureInfo.InvariantCulture.DateTimeFormat.LongDatePattern, CultureInfo.InvariantCulture.DateTimeFormat)})
";
            md += string.Join(@"
", groups.Select(s =>
            {
                string ret = link
                    ? $@"  - [`{s.GetAtt<GroupAttribute>().Name}`]({s.GetAtt<GroupAttribute>().Name}/{(trimLinks ? "" : $"index{suffix}")}): {s.GetAtt<DescriptionAttribute>().Description}
"
                    : $@"  - `{s.GetAtt<GroupAttribute>().Name}`: {s.GetAtt<DescriptionAttribute>().Description}
";
                ret += string.Join(@"
", s.GetCommands().Select(a =>
                {
                    if (link)
                        return
                            $"    - [`{a.GetAtt<CommandAttribute>().Name}`]({s.GetAtt<GroupAttribute>().Name}/{a.GetAtt<CommandAttribute>().Name}{(trimLinks ? "" : suffix)}): {a.GetAtt<DescriptionAttribute>().Description}";
                    return
                        $"    - `{a.GetAtt<CommandAttribute>().Name}`: {a.GetAtt<DescriptionAttribute>().Description}";
                }));
                return ret;
            }));
            md += @"
- Powerful configuration
  - Custom XML-based layered database
  - Toggle every single command
  - Custom prefixes
- Easy to develop
  - Understandable solution structure
  - Lots of testing
  - Few external dependencies (you don't need PHP, SQL, Node.JS)
  - No costs due to API keys (eg Google Translate)
  - Modern library
  - Every group, command and parameter is documented in [help]
- Fast
  - My instance runs just fine on a RasPi with minimal specs";
            return md;
        }
    }
}