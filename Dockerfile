FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env
WORKDIR /app

COPY . ./

RUN dotnet publish Bot/Bot.csproj -c Release -o out -p:DefineConstants="NO_CLI"
RUN rm out/Bot

FROM mcr.microsoft.com/dotnet/runtime:5.0-buster-slim-arm64v8
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "Bot.dll"]
