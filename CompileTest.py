#!/usr/bin/python
import itertools
import subprocess

S = ['NO_TIMED_BAN', 'NO_NSFW', 'NO_CLI', 'VERBOSE']

maxRuns = pow(2, len(S))
totalRuns = 0

for i in range(len(S)+1):
    for c in itertools.combinations(S, i):
        totalRuns += 1
        conf = ';'.join(c)
        print(conf + "(" + str(totalRuns) + "/" + str(maxRuns) + ")")
        subprocess.run(["dotnet", "publish", "Bot/Bot.csproj", "-c", "Release", "-p:DefineConstants=\"" + conf + "\"", "-p:WarningLevel=0"])
