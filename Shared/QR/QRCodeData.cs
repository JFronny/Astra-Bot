using System;
using System.Collections;
using System.Collections.Generic;

namespace Shared.QR
{
    public class QrCodeData : IDisposable
    {
        public QrCodeData(int version)
        {
            Version = version;
            int size = ModulesPerSideFromVersion(version);
            ModuleMatrix = new List<BitArray>();
            for (int i = 0; i < size; i++)
                ModuleMatrix.Add(new BitArray(size));
        }

        public List<BitArray> ModuleMatrix { get; set; }

        public int Version { get; private set; }

        public void Dispose()
        {
            ModuleMatrix = null;
            Version = 0;
        }

        private static int ModulesPerSideFromVersion(int version) => 21 + (version - 1) * 4;
    }
}