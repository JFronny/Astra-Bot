using System;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Drawing.Processing;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace Shared.QR
{
    public class QrCode : IDisposable
    {
        public QrCode(QrCodeData data) => QrCodeData = data;

        private QrCodeData QrCodeData { get; }

        public void Dispose() => QrCodeData.Dispose();

        public Image GetGraphic(int pixelsPerModule) => GetGraphic(pixelsPerModule, Color.Black, Color.White);

        private Image GetGraphic(int pixelsPerModule, Color darkColor, Color lightColor, bool drawQuietZones = true)
        {
            int size = (QrCodeData.ModuleMatrix.Count - (drawQuietZones ? 0 : 8)) * pixelsPerModule;
            int offset = drawQuietZones ? 0 : 4 * pixelsPerModule;

            Image image = new Image<Rgba32>(size, size);
            image.Mutate(s =>
            {
                for (int x = 0; x < size + offset; x = x + pixelsPerModule)
                for (int y = 0; y < size + offset; y = y + pixelsPerModule)
                {
                    bool module =
                        QrCodeData.ModuleMatrix[(y + pixelsPerModule) / pixelsPerModule - 1][
                            (x + pixelsPerModule) / pixelsPerModule - 1];

                    s.Fill(module ? darkColor : lightColor,
                        new Rectangle(x - offset, y - offset, pixelsPerModule, pixelsPerModule));
                }
            });
            return image;
        }
    }
}