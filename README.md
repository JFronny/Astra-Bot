# Astra-Bot
A powerful multi-purpose discord bot!
[![Discord](https://img.shields.io/discord/466965965658128384?label=Discord)](https://discord.gg/UjhHBqt)
[![Invite](https://img.shields.io/badge/-Invite-success)](https://discordapp.com/oauth2/authorize?client_id=644947101041557554&scope=bot&permissions=4294443006)

### Features
Read [this](https://jfronny.gitlab.io/Astra-Bot) for a somewhat complete list of features (that page is updated automatically by [MDExtractor](https://gitlab.com/JFronny/Astra-Bot/-/tree/master/MDExtractor))

### Projects in this Repository
The main projects are "Shared" (somewhat reusable snippets, independent from the actual bot code for better structure) and "Bot" (the main bot project)

There is also [MDExtractor](https://gitlab.com/JFronny/Astra-Bot/-/tree/master/MDExtractor). This is used to generate the documentation with reflection. MDExtractor is executed with [this](https://gitlab.com/JFronny/Astra-Bot/-/blob/master/.gitlab-ci.yml) script after any pushed commit

All other projects are experiments for some features. They are stand-alone projects that can be ran independently from other projects